<?php
return[
    'our' => 'Our',
    'advantages' => 'advantages',
    'back' => 'Request a quote',
    'call' => 'Let`s talk',
    'more' => 'See articles',
    'discount' => 'Discount, please',
    'lang' => 'English',
    'logos' => 'Logos',
    'portfolio' => 'Portfolio',
    'team' => 'Team',
    'philosophy' => 'Philosophy',
    'reason'=>'Forethought',
    'phone' => 'Phone',
    'article'=>'Articles',
    'popular'=>'Popular articles',
    'last_ones'=>'Last articles',
    'all_articles'=>'All articles',
    'offers' => 'Offers',
    'contacts' => 'Contacts',
    'reserve'=>'All rights reserved',
    'policy'=> 'Terms and conditions',
    'join_us_luke'=>'I want to join'
]
?>
