@extends('layouts.app')
@section('content')
    <div id="full page">

        <div class="leaves">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
        <!-- MAIN  -->
        <section class="main" style="">
            <div class="container grid" style="overflow: visible">
                <div class="textsss">
                    @if(session('locale') == 'ru' || session('locale') == null)
                        <h1>{{ setting('glavnaya-stranica.welcome') }}</h1>
                        <p>{{ setting('glavnaya-stranica.welcome_desc') }}</p>
                    @elseif(session('locale') == 'en')
                        <h1>{{ setting('glavnaya-stranica-en.welcome') }}</h1>
                        <p>{{ setting('glavnaya-stranica-en.welcome_desc') }}</p>
                    @elseif(session('locale') == 'kz')
                        <h1>{{ setting('glavnaya-stranica-kz.welcome') }}</h1>
                        <p>{{ setting('glavnaya-stranica-kz.welcome_desc') }}</p>
                    @endif
                </div>
                <div class="balls" id="balls">
                    <div class="ball"></div>
                    <div class="ball"></div>
                    <div class="ball"></div>
                    <div class="ball"></div>
                    <div class="ball mains"></div>
                    <div class="ball"></div>
                    <div class="ball"></div>
                    <div class="ball"></div>
                    <div class="ball"></div>
                    <div class="ball"></div>
                </div>
                {{--            <a href="#" class="btn">Смотреть проекты</a>--}}
            </div>
        </section>
        <div class="content" style="background: none">
            <!-- COMPANY  -->
            <section class="company">
                <div class="container">
                    <div class="light-ball"></div>
                    @if(session('locale') == 'ru' || session('locale') == null)
                        <h2>{{ setting('glavnaya-stranica.doing') }}</h2>
                        <p>{{ setting('glavnaya-stranica.doing_desc') }}</p>
                    @elseif(session('locale') == 'en')
                        <h2>{{ setting('glavnaya-stranica-en.doing') }}</h2>
                        <p>{{ setting('glavnaya-stranica-en.doing_desc') }}</p>
                    @elseif(session('locale') == 'kz')
                        <h2>{{ setting('glavnaya-stranica-kz.doing') }}</h2>
                        <p>{{ setting('glavnaya-stranica-kz.doing_desc') }}</p>

                    @endif
                    <div class="grid grid-3">
                        @foreach($offers as $offer)
                            <div>
                                <h3>{{ $offer->title }}</h3>
                                @foreach($offer->subOffer as $subOffer)
                                    <p>{{ $subOffer->title }}</p>
                                @endforeach
                                {{--                    <p>Интерфейсы</p>--}}
                                {{--                    <p>Дизайн мобильных приложений</p>--}}
                                {{--                    <p>Дизайн сайтов</p>--}}
                            </div>
                        @endforeach
                        {{--                <div>--}}
                        {{--                    <h3>ТЕКСТЫ И ПЕРЕВОДЫ</h3>--}}
                        {{--                    <p>Копирайтинг</p>--}}
                        {{--                    <p>Нейминг и Слоганы</p>--}}
                        {{--                    <p>Переводы</p>--}}
                        {{--                    <p>Продающие тексты</p>--}}
                        {{--                    <p>Редактирование и Корректура</p>--}}
                        {{--                    <p>Рерайтинг</p>--}}
                        {{--                    <p>Стихи, Песни и Проза</p>--}}
                        {{--                    <p>Сценарии</p>--}}
                        {{--                    <p>Транскрибация</p>--}}
                        {{--                </div>--}}
                        {{--                <div>--}}
                        {{--                    <h3>SEO</h3>--}}
                        {{--                    <p>Контекстная реклама</p>--}}
                        {{--                    <p>Маркетинговый анализ</p>--}}
                        {{--                    <p>Поисковые системы</p>--}}
                        {{--                    <p>Социальные сети</p>--}}
                        {{--                </div>--}}
                        {{--                <div>--}}
                        {{--                    <h3>РАЗРАБОТКА</h3>--}}
                        {{--                    <p>HTML-верстка</p>--}}
                        {{--                    <p>Веб-программирование</p>--}}
                        {{--                    <p>Интернет-магазины</p>--}}
                        {{--                    <p>Системы управления (CMS)</p>--}}
                        {{--                    <p>Тестирование сайтов</p>--}}
                        {{--                </div>--}}
                        {{--                <div>--}}
                        {{--                    <h3>ПОЛИГРАФИЯ и АЙДЕНТИКА</h3>--}}
                        {{--                    <p>Верстка полиграфии</p>--}}
                        {{--                    <p>Дизайн продукции</p>--}}
                        {{--                    <p>Логотипы и Знаки</p>--}}
                        {{--                    <p>Наружная реклама</p>--}}
                        {{--                    <p>Фирменный стиль</p>--}}
                        {{--                </div>--}}
                    </div>
                </div>
            </section>
            <section class="working">
                <div class="container grid">
                    <img src="img/map.svg" alt="Карта Казахстана">
                    <h2 class="title">Работаем на выезд <br> и на вылет</h2>
                </div>
            </section>

{{--            <section class="ours">--}}
{{--                <div class="container">--}}
{{--                    <h2 class="title">О нас</h2>--}}
{{--                    <div class="ours-content">--}}
{{--                    <div class="ours-content-top">--}}
{{--                        <div class="ours-item-top" style="background-image: {{asset('/storage/'.setting('glavnaya-stranica.about_us_image_1'))}}">--}}
{{--                            <img src=" {{asset('/storage/'.setting('glavnaya-stranica.about_us_image_1'))}}" style="border-radius: 49%; width: 206px; margin: 23px; height: 206px">--}}
{{--                        </div>--}}
{{--                        <div class="ours-item-top" style="background-image: {{setting('glavnaya-stranica.about_us_image_1')}}">--}}
{{--                            <img src=" {{asset('/storage/'.setting('glavnaya-stranica.about_us_image_2'))}}" style="border-radius: 49%; width: 206px; margin: 23px; height: 206px">--}}
{{--                        </div>--}}
{{--                        <div class="ours-item-top" style="background-image: {{setting('glavnaya-stranica.about_us_image_1')}}">--}}
{{--                            <img src=" {{asset('/storage/'.setting('glavnaya-stranica.about_us_image_3'))}}" style="border-radius: 49%; width: 206px; margin: 23px; height: 206px">--}}
{{--                        </div>--}}
{{--                        --}}{{-- <div class="ours-item-top-text">--}}
{{--                            <p>Ответственно подходим к работенад Вашим проектом</p>--}}
{{--                        </div> --}}
{{--                    </div>--}}
{{--                    <div class="ours-content-mid">--}}
{{--                        <div class="ours-item-mid">--}}
{{--                        </div>--}}
{{--                        <div class="ours-item-mid-text">--}}

{{--                        </div>--}}
{{--                        <div class="ours-item-mid"></div>--}}
{{--                    </div>--}}
{{--                    <div class="ours-content-bottom">--}}
{{--                        <div class="ours-item-bottom">--}}
{{--                            <img src=" {{asset('/storage/'.setting('glavnaya-stranica.about_us_image_4'))}}" style="border-radius: 49%; width: 206px; margin: 23px; height: 206px">--}}
{{--                        </div>--}}
{{--                        <div class="ours-item-bottom">--}}
{{--                            <img src=" {{asset('/storage/'.setting('glavnaya-stranica.about_us_image_5'))}}" style="border-radius: 49%; width: 206px; margin: 23px; height: 206px">--}}
{{--                        </div>--}}
{{--                        <div class="ours-item-bottom">--}}
{{--                            <img src=" {{asset('/storage/'.setting('glavnaya-stranica.about_us_image_6'))}}" style="border-radius: 49%; width: 206px; margin: 23px; height: 206px">--}}
{{--                        </div>--}}
{{--                        <div class="ours-texts">--}}
{{--                            <div class="ours-texts-top">--}}
{{--                                @if(session('locale') == 'ru' || session('locale') == null)--}}
{{--                                {{ setting('glavnaya-stranica.about_us') }}--}}
{{--                                @elseif(session('locale') == 'en')--}}
{{--                                    {{ setting('glavnaya-stranica-en.about_us') }}--}}
{{--                                @elseif(session('locale') == 'kz')--}}
{{--                                    {{ setting('glavnaya-stranica-kz.about_us') }}--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                            <div class="ours-text-mid">--}}
{{--                                @if(session('locale') == 'ru' || session('locale') == null)--}}
{{--                                    {{ setting('glavnaya-stranica.about_us_text') }}--}}
{{--                                @elseif(session('locale') == 'en')--}}
{{--                                    {{ setting('glavnaya-stranica-en.about_us_text') }}--}}
{{--                                @elseif(session('locale') == 'kz')--}}
{{--                                    {{ setting('glavnaya-stranica-kz.about_us_text') }}--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                </div>--}}
{{--            </section>--}}
            <section class="advantages">
                <div class="container grid">
                    <div>
                        <h2>@lang('main.our') <br> @lang('main.advantages')</h2>
                        @if(session('locale') == 'ru' || session('locale') == null)

                            <p>{{ setting('glavnaya-stranica.advantages') }}</p>
                            <a href="#" class="btn btn-tel">@lang('main.call')</a>
                        @elseif(session('locale') == 'en')
                            <p>{{ setting('glavnaya-stranica-en.advantages') }}</p>
                            <a href="#" class="btn btn-tel">@lang('main.call')</a>
                        @elseif(session('locale') == 'kz')
                            <p>{{ setting('glavnaya-stranica-kz.advantages') }}</p>
                            <a href="#" class="btn btn-tel">@lang('main.call')</a>

                        @endif
                    </div>
                    <div class="grid">
                        <div class="card">
                            <span class="icon-integration"></span>
                            @if(session('locale') == 'ru' || session('locale') == null)
                                <h3>{{setting('glavnaya-stranica.advantage-first')}}</h3>
                                <p>{{ setting('glavnaya-stranica.integrate') }}</p>
                            @elseif(session('locale') == 'en')
                                <h3>{{setting('glavnaya-stranica-en.advantage-first')}}</h3>
                                <p>{{ setting('glavnaya-stranica-en.integrate') }}</p>
                            @elseif(session('locale') == 'kz')
                                <h3>{{setting('glavnaya-stranica-kz.advantage-first')}}</h3>
                                <p>{{ setting('glavnaya-stranica-kz.integrate') }}</p>

                            @endif
                        </div>
                        <div class="card">
                            <span class="icon-communication"></span>
                            @if(session('locale') == 'ru' || session('locale') == null)
                                <h3>{{setting('glavnaya-stranica.advantage-second')}}</h3>
                                <p>{{ setting('glavnaya-stranica.multilang') }}</p>
                            @elseif(session('locale') == 'en')
                                <h3>{{setting('glavnaya-stranica-en.advantage-second')}}</h3>
                                <p>{{ setting('glavnaya-stranica-en.multilang') }}</p>
                            @elseif(session('locale') == 'kz')
                                <h3>{{setting('glavnaya-stranica-kz.advantage-second')}}</h3>
                                <p>{{ setting('glavnaya-stranica-kz.multilang') }}</p>
                            @endif
                        </div>
                        <div class="card">
                            <span class="icon-communication"></span>
                            @if(session('locale') == 'ru' || session('locale') == null)
                                <h3>{{ setting('glavnaya-stranica.advantage-third') }}</h3>
                                <p>{{ setting('glavnaya-stranica.complex_foms') }}</p>
                            @elseif(session('locale') == 'en')
                                <h3>{{ setting('glavnaya-stranica-en.advantage-third') }}</h3>
                                <p>{{ setting('glavnaya-stranica-en.complex_foms') }}</p>
                            @elseif(session('locale') == 'kz')
                                <h3>{{ setting('glavnaya-stranica-kz.advantage-third') }}</h3>
                                <p>{{ setting('glavnaya-stranica-kz.complex_foms') }}</p>

                            @endif
                        </div>
                        <div class="card">
                            <span class="icon-like"></span>
                            @if(session('locale') == 'ru' || session('locale') == null)
                                <h3>{{ setting('glavnaya-stranica.advantage-fourth') }}</h3>
                                <p>{{ setting('glavnaya-stranica.individual') }}</p>
                            @elseif(session('locale') == 'en')
                                <h3>{{ setting('glavnaya-stranica-en.advantage-fourth') }}</h3>
                                <p>{{ setting('glavnaya-stranica-en.individual') }}</p>
                            @elseif(session('locale') == 'kz')
                                <h3>{{ setting('glavnaya-stranica-kz.advantage-fourth') }}</h3>
                                <p>{{ setting('glavnaya-stranica-kz.individual') }}</p>

                            @endif
                        </div>
                    </div>
                </div>
            </section>
            <section class="article">
                <div class="container">
                    @if(session('locale') == 'ru' || session('locale') == null)
                        <h2>{{ setting('glavnaya-stranica.articles_main') }}</h2>
                        <p>{{ setting('glavnaya-stranica.articles_main_desc') }}</p>
                    @elseif(session('locale') == 'en')
                        <h2>{{ setting('glavnaya-stranica-en.articles_main') }}</h2>
                        <p>{{ setting('glavnaya-stranica-en.articles_main_desc') }}</p>
                     @elseif(session('locale') == 'kz')
                        <h2>{{ setting('glavnaya-stranica-kz.articles_main') }}</h2>
                        <p>{{ setting('glavnaya-stranica-kz.articles_main_desc') }}</p>

                    @endif
                    <div class="grid">
                            <div>
                                <a class="article__card flex" href="{{ route('article', $firstPost->slug) }}">
                                    <div class="article__content">
                                        <h3>
                                            {{ $firstPost->title }}
                                        </h3>
                                        <p>
                                            {!! Str::words($firstPost->excerpt, '14') !!}
                                        </p>
                                    </div>
                                    <img style="max-width: 260px; height: 240px; border-radius: 10px"
                                         src="{{ asset('storage/'.$firstPost->image) }}" alt="">
                                </a>
                            </div>
                            <div>
                                <a href="{{ route('article', $secondPost->slug) }}">
                                    <h3>{{ $secondPost->title }}</h3>
                                    <p>{!! Str::words($secondPost->excerpt, '14') !!}</p>
                                    <img src="{{ asset('storage/'.$secondPost->image) }}" alt="">
                                </a>
                            </div>

                        <div>
                            <a class="article__card flex" href="{{ route('article', $thirdPost->slug) }}">
                                <div class="article__content">
                                    <h3>{{ $thirdPost->title }}</h3>
                                    <p>{!! Str::words($thirdPost->excerpt, '14') !!}</p>
                                </div>
                                <img src="{{ asset('storage/'.$thirdPost->image) }}" alt="">
                            </a>
                        </div>
                    </div>
                    <center>
                        <a href="{{ route('posts') }}" class="btn btn-more">@lang('main.more')</a>
                    </center>
                </div>
            </section>
        </div>
    </div>
@endsection
