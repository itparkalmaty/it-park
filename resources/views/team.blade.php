@extends('layouts.app')
@section('content')
    <section class="team content">
        <div class="container">
            <h1>@lang('main.team')</h1>
        </div>
    </section>
    <section class="team-wrapper">
        <div class="content team-content">
            <div class="container">
                <div class="team-content">
                    <h2>{{ setting('komanda.team') }}</h2>
                    <div class="team-banner">
                        @foreach($teamMates as $teamMate)
{{--                            <img class="team-main" src="{{ ('storage/'.$teamMate->iamge) }}" alt="">--}}
                            @break
                        @endforeach
                        <div class="team__peoples">
                            <div class="team-people">
                                @foreach($teamMates as $teamMate)
{{--                                    <img class="people-tab people-sm-photo" src="{{ ('storage/'.$teamMate->iamge) }}"--}}
{{--                                         alt="">--}}
                                @endforeach
                                {{--                                <div class="people-tab people-sm-photo">--}}
                                {{--                                    <img src="img/people.png" alt="">--}}
                                {{--                                </div>--}}
                                {{--                                <div>--}}
                                {{--                                    <img src="img/people-3.png" alt="">--}}
                                {{--                                </div>--}}
                                {{--                                <div>--}}
                                {{--                                    <img src="img/people-4.png" alt="">--}}
                                {{--                                </div>--}}
                                {{--                                <div>--}}
                                {{--                                    <img src="img/people-5.png" alt="">--}}
                                {{--                                </div>--}}
                                {{--                                <div>--}}
                                {{--                                    <img src="img/people-6.png" alt="">--}}
                                {{--                                </div>--}}
                                {{--                                <div>--}}
                                {{--                                    <img src="img/people-7.png" alt="">--}}
                                {{--                                </div>--}}
                            </div>
{{--                            @foreach($teamMates as $teamMate)--}}
{{--                                <div class="team-personal">--}}
{{--                                    <div class="personal__top">--}}
{{--                                        <h3>{{ $teamMate->position }}</h3>--}}
{{--                                        <span>{{ $teamMate->experience }}</span>--}}
{{--                                    </div>--}}
{{--                                    <h2>{{ $teamMate->name }}</h2>--}}
{{--                                    <p>"{{ $teamMate->description }}"</p>--}}
{{--                                    <div class="team-personal-icon">--}}
{{--                                        <span class="icon-contact"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            @endforeach--}}
                        </div>
                        <center>
                            <a href="#" class="btn btn-lg">@lang('main.join_us_luke')</a>
                        </center>
                    </div>
                </div>
            </div>
            <section class="philosophy">
                <div class="container grid">
                    <div>
                        <img src="img/headstone.jpg" alt="">
                    </div>
                    <div>
                        <h3>@lang('main.philosophy')</h3>
                        @if(session('locale') == 'ru' || session('locale') == null)
                            <p>{{ setting('komanda.philosophy') }}</p>
                        @elseif(session('locale') == 'en')
                            <p>{{ setting('komanda-en.philosophy') }}</p>
                        @elseif(session('locale') == 'kz')
                            <p>{{ setting('komanda-kz.philosophy') }}</p>

                        @endif
                    </div>
                    <div>
                        <h3>@lang('main.reason')</h3>
                        @if(session('locale') == 'ru' || session('locale') == null)
                            <p>
                                {{ setting('komanda.ideas') }}
                            </p>
                        @elseif(session('locale') == 'en')
                            <p>
                                {{ setting('komanda-en.ideas') }}
                            </p>
                        @elseif(session('locale') == 'kz')
                            <p>
                                {{ setting('komanda-kz.ideas') }}
                            </p>
                        @endif
                    </div>
                    <div>
                        <img src="img/boom.jpg" alt="">
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
