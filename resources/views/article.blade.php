@extends('layouts.app')
@section('content')
<div class="content content-article">
    <section class="article">
        <div class="container">
            <div class="article_content">
            <h1>{!! $post->title !!}</h1>
            <img src="{!! asset('storage/'.$post->image) !!}" alt="Который вызывает у некторых людей недоумение при попытках">
                <p>{!! $post->body !!}</p>
{{--            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores odit eos et nisi nostrum ullam facere voluptas unde tempora. Impedit mollitia, minima ea quasi velit earum non omnis distinctio dolore. Suscipit magni harum consequatur quia?</p>--}}
        </div>
        </div>
    </section>
</div>
@endsection
