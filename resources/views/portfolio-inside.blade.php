@extends('layouts.app')
@section('content')
{{--    @dd(1)--}}
<div class="content content-portfolio-inside">
    <section class="portfolio-inside">
        <div class="container">
            <div class="portfolio-nav">
                @foreach($projectTypo as $projecto)
                <div class="portfolio-item active">
                    <a href="{{ route('outside', $projecto->id) }}">{{ $projecto->title }}</a>
                </div>
                @endforeach
{{--                <div class="portfolio-item">--}}
{{--                    <a href="#">Мобильное приложение</a>--}}
{{--                </div>--}}
{{--                <div class="portfolio-item">--}}
{{--                    <a href="#">Логотипы</a>--}}
{{--                </div>--}}
{{--                <div class="portfolio-item">--}}
{{--                    <a href="#">UX|UI</a>--}}
{{--                </div>--}}
            </div>
            <h1>{!! $project->description !!}</h1>
            <div class="portfolio-inside-content">
                <div class="portfolio-inside-grid grid">
{{--                    @dd($project)--}}
                    <img src={!! asset('storage/'.$project->image) !!} alt="Шаблон">
                    <div class="portfolio-lists">
                        <p>{!! $project->title !!}</p>
                        <p>{!! $project->front_tools !!}</p>
                        <p>{!! $project->back_tools !!}</p>
                    </div>
                </div>
                <p class="portfolio-descr">{!! $project->long_description !!}</p>
            </div>
        </div>
    </section>
</div>
@endsection
