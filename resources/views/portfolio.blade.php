@extends('layouts.app')
@section('content')
    <div class="content content-portfolio-inside">
        <section class="portfolio">
            <div class="container">
                <h1>Портфолио</h1>
                <p class="portfolio-subtitle">
                    Подходим к созданию Вашего проекта с применением ярких и эффективных решений
                </p>
                <div class="portfolio-nav">
                    @foreach($projectTypo as $project)
                    <div class="portfolio-item active">
                        <a href="{{ route('inside', $project->id) }}">{{ $project->title }}</a>
                    </div>
                    @endforeach
{{--                    <div class="portfolio-item">--}}
{{--                        <a href="#">Мобильное приложение</a>--}}
{{--                    </div>--}}
{{--                    <div class="portfolio-item">--}}
{{--                        <a href="#">Логотипы</a>--}}
{{--                    </div>--}}
{{--                    <div class="portfolio-item">--}}
{{--                        <a href="#">UX|UI</a>--}}
{{--                    </div>--}}
                </div>
                <div class="portfolio-slider-wrap">
                    <img src="{{ asset('img/mac.png') }}" alt="" class="mac">
                    <div class="portfolio-sliders">
                        <div class="swiper-wrapper">
                            {{--                            @dd($projects)--}}
                            @foreach($projects as $project)
                                <div class="portfolio-slider swiper-slide">
                                    <a href="{{ route('inside', $project->id) }}">
                                        <img src={{ asset('storage/'.$project->image) }} alt="">
                                    </a>
                                </div>
                            @endforeach
                            {{--                    <a href="#" class="portfolio-slider swiper-slide">--}}
                            {{--                        <img src={{ asset('img/template.jpg') }} alt="">--}}
                            {{--                    </a>--}}
                            {{--                    <a href="#" class="portfolio-slider swiper-slide">--}}
                            {{--                        <img src={{ asset('img/template.jpg') }} alt="">--}}
                            {{--                    </a>--}}
                            {{--                    <a href="#" class="portfolio-slider swiper-slide">--}}
                            {{--                        <img src={{ asset('img/template.jpg') }} alt="">--}}
                            {{--                    </a>--}}
                        </div>
                    </div>
                    <div class="portfolio-slider-back">
                        <img src={{ asset('img/arrow-back.svg') }} alt="">
                    </div>
                    <div class="portfolio-slider-next">
                        <img src={{ asset('img/Arrow-next.svg') }} alt="">
                    </div>
                </div>
            </div>
        </section>
        {{--        <section class="landing">--}}
        {{--            <div class="container">--}}
        {{--                <h2>LandingPage</h2>--}}
        {{--                <p>{{ setting('portfolio.landing') }}</p>--}}
        {{--                <div class="grid">--}}
        {{--                    @foreach($projects as $project)--}}
        {{--                        @if($project->projectType->slug=='landing-page')--}}
        {{--                        <div class="maket__item">--}}
        {{--                            <img src="img/furniture.jpg" alt="">--}}
        {{--                        </div>--}}
        {{--                        <div class="maket__item">--}}
        {{--                            <img src="{{ ('storage/').$project->image }}" alt="">--}}
        {{--                        </div>--}}
        {{--                            <div class="maket__item">--}}
        {{--                                <img src="{{ ('storage/').$project->image }}" alt="">--}}
        {{--                                <div class="landing__info">--}}
        {{--                                    <h3> {{ $project->title }}--}}
        {{--                                        --}}{{--                                    Презентация для компании “ТеплоКамень”--}}
        {{--                                    </h3>--}}
        {{--                                    <p>{{ $project->description }}</p>--}}
        {{--                                    <a href="#" class="landing-btn">Хочу похожую работу</a>--}}
        {{--                                </div>--}}
        {{--                            </div>--}}
        {{--                        <div class="maket__item">--}}
        {{--                            <img src="img/white.jpg" alt="">--}}
        {{--                        </div>--}}
        {{--                        @endif--}}
        {{--                    @endforeach--}}
        {{--                </div>--}}
        {{--                <div class="mobile-application">--}}
        {{--                    <div class="works-container">--}}
        {{--                        <div class="swiper-wrapper">--}}
        {{--                            <div class="swiper-slide">--}}
        {{--                                <div class="grid">--}}
        {{--                                    @foreach($projects as $project)--}}
        {{--                                        @if($project->projectType->slug=='mobilnye-prilozheniya')--}}
        {{--                                            <div>--}}
        {{--                                                <h2>мобильные приложения</h2>--}}
        {{--                                                <h3>{{ $project->title }}</h3>--}}
        {{--                                                <p>{{ $project->description }}</p>--}}
        {{--                                                <a class="btn" href="#">Смотреть ещё</a>--}}
        {{--                                            </div>--}}
        {{--                                            <div>--}}
        {{--                                                <img src="{{ ('storage/').$project->image }}" alt="">--}}
        {{--                                                <a href="#">Следующая работа <span class="icon-right"></span></a>--}}
        {{--                                            </div>--}}
        {{--                                        @endif--}}
        {{--                                    @endforeach--}}
        {{--                                </div>--}}
        {{--                            </div>--}}
        {{--                        </div>--}}
        {{--                        @endif--}}
        {{--                    @endforeach--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </section>--}}
        {{--    </div>--}}
    </div>
@endsection
