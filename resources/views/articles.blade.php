@extends('layouts.app')
@section('content')
    <div class="content content-articles">
        <section class="articles-catalog">
            <div class="container">
                <h1>@lang('main.article')</h1>
                <p>{{ setting('stati.articles') }}</p>
                <h2>@lang('main.popular')</h2>
                @foreach($popularPosts as $post)
                    <a href="{{ route('article', $post->slug) }}" class="grid grid-news">
                        <img src="{{ ('storage/').$post->image }}" alt="">
                        <div>
                            {{--                    @dd(Carbon\Carbon::parse($post->created_at)->translatedFormat('F') )--}}
                            <h4>{{ $post->title }}</h4>
                            <p>{{ $post->excerpt }}</p>
                            <time>{{ Carbon\Carbon::parse($post->created_at)->day }} {{ Carbon\Carbon::parse($post->created_at)->translatedFormat('F') }}
                                , {{ Carbon\Carbon::parse($post->created_at)->year }}</time>
                        </div>
                    </a>
                @endforeach
                <h2>@lang('main.last_ones')</h2>
                <div class="articles__last">
                    <div class="grid grid-3">
                        @foreach($newPosts as $post)
                            <a href="{{ route('article', $post->slug) }}" class="item">
                                <img src="{{ ('storage/').$post->image }}" alt="">
                                <h4>{{ Str::words($post->excerpt, '8') }}</h4>
                                <time>{{ Carbon\Carbon::parse($post->created_at)->day }} {{ Carbon\Carbon::parse($post->created_at)->translatedFormat('F') }}
                                    , {{ Carbon\Carbon::parse($post->created_at)->year }}</time>
                            </a>
                        @endforeach
                        {{--                <a href="#" class="item">--}}
                        {{--                    <img src="img/humans.png" alt="">--}}
                        {{--                    <h4>Который вызывает у некторых людей недоумение при попытках</h4>--}}
                        {{--                    <time>21 Февраля, 2021</time>--}}
                        {{--                </a>--}}
                        {{--                <a href="#" class="item">--}}
                        {{--                    <img src="img/boxes.png" alt="">--}}
                        {{--                    <h4>Который вызывает у некторых людей недоумение при попытках</h4>--}}
                        {{--                    <time>14 Февраля, 2021</time>--}}
                        {{--                </a>--}}
                        {{--                <a href="#" class="item">--}}
                        {{--                    <img src="img/cards.png" alt="">--}}
                        {{--                    <h4>Который вызывает у некторых людей недоумение при попытках</h4>--}}
                        {{--                    <time>14 Февраля, 2021</time>--}}
                        {{--                </a>--}}
                        {{--                <a href="#" class="item">--}}
                        {{--                    <img src="img/image.png" alt="">--}}
                        {{--                    <h4>Который вызывает у некторых людей недоумение при попытках</h4>--}}
                        {{--                    <time>14 Февраля, 2021</time>--}}
                        {{--                </a>--}}
                        {{--                <a href="#" class="item">--}}
                        {{--                    <img src="img/instagram.png" alt="">--}}
                        {{--                    <h4>Который вызывает у некторых людей недоумение при попытках</h4>--}}
                        {{--                    <time>14 Февраля, 2021</time>--}}
                        {{--                </a>--}}
                    </div>
                </div>
            </div>
        </section>
        <section class="all-articles">
            <div class="container">
                <h2>@lang('main.all_articles')</h2>
                <div class="grid grid-3">
                    @foreach($posts as $post)
                        <a href="{{ route('article', $post->slug) }}" class="item">
                            <img src="{{ ('storage/').$post->image }}" alt="">
                            <h4>{{ Str::words($post->excerpt, '8') }}</h4>
                            <time>{{ Carbon\Carbon::parse($post->created_at)->day }} {{ Carbon\Carbon::parse($post->created_at)->translatedFormat('F') }}
                                , {{ Carbon\Carbon::parse($post->created_at)->year }}</time>
                        </a>
                    @endforeach
                    {{--                <a href="#" class="item">--}}
                    {{--                    <img src="img/humans.png" alt="">--}}
                    {{--                    <h4>Который вызывает у некторых людей недоумение при попытках</h4>--}}
                    {{--                    <time>21 Февраля, 2021</time>--}}
                    {{--                </a>--}}
                    {{--                <a href="#" class="item">--}}
                    {{--                    <img src="img/boxes.png" alt="">--}}
                    {{--                    <h4>Который вызывает у некторых людей недоумение при попытках</h4>--}}
                    {{--                    <time>14 Февраля, 2021</time>--}}
                    {{--                </a>--}}
                    {{--                <a href="#" class="item">--}}
                    {{--                    <img src="img/cards.png" alt="">--}}
                    {{--                    <h4>Который вызывает у некторых людей недоумение при попытках</h4>--}}
                    {{--                    <time>14 Февраля, 2021</time>--}}
                    {{--                </a>--}}
                    {{--                <a href="#" class="item">--}}
                    {{--                    <img src="img/image.png" alt="">--}}
                    {{--                    <h4>Который вызывает у некторых людей недоумение при попытках</h4>--}}
                    {{--                    <time>14 Февраля, 2021</time>--}}
                    {{--                </a>--}}
                    {{--                <a href="#" class="item">--}}
                    {{--                    <img src="img/instagram.png" alt="">--}}
                    {{--                    <h4>Который вызывает у некторых людей недоумение при попытках</h4>--}}
                    {{--                    <time>14 Февраля, 2021</time>--}}
                    {{--                </a>--}}
                </div>
{{--                <center>--}}
{{--                    <a href="#" class="btn btn-more">Показать ещё</a>--}}
{{--                </center>--}}
            </div>
        </section>
    </div>
@endsection
