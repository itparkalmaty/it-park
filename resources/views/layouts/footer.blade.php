<footer class="footer">
    <div class="container">
        <div class="grid grid-4">
            <div class="flex">
                {{-- <a href="#">
                    <img src="img/icons/facebook.svg" alt="Facebook">
                    <h3>100K</h3>
                    <p>Facebook</p>
                </a> --}}
                <a href="https://www.instagram.com/itparkalmaty/">
                    <img src="{{ asset('img/icons/instagram.svg') }}" alt="Instagram">
                    {{-- <h3>235K</h3> --}}
                    <p>Instagram</p>
                </a>
            </div>
            <div>
                <h3>@lang('main.contacts')</h3>
                <ul>
                    <li>{{ $contacts->address }}</li>
                    <li><a href="tel:{{ $contacts->phone }}">{{ $contacts->phone }}</a></li>
                    <li><a href="mailto:{{ $contacts->email }}">{{ $contacts->email }}</a></li>
                </ul>
            </div>
            <div>
                <a href='https://2gis.kz/almaty/firm/70000001051294228' target="_blank" id="map" style="width:100%; height:400px">
                <img src="img/map.png" alt="">
                </a>
            </div>
        </div>
        <div class="footer-mid flex">
            <img src="{{ asset('img/footer-logo.png') }}" alt="IT Park" class="footer-logo" title="IT park">
            @if(session('locale') == 'ru' || session('locale') == null)
                <p>
                    {{ setting('glavnaya-stranica.footer') }}
                </p>
            @elseif(session('locale') == 'en')
                {{ setting('glavnaya-stranica-en.footer') }}
            @endif
        </div>
    </div>
    <div class="footer__bottom">
        <div class="container grid grid-3 footer-bottom">
            {{--            <div class="language">--}}
            {{--                <div class="language__select">--}}
            {{--                    <img src="img/eng.png" alt="">--}}
            {{--                    <span>English <span class="icon-arrow-down"></span></span>--}}
            {{--                </div>--}}
            {{--                <div class="language__list language-footer">--}}
            {{--                    <div class="language__eng">--}}
            {{--                        <img src="img/eng.png" alt="">--}}
            {{--                        <span>English</span>--}}
            {{--                    </div>--}}
            {{--                    <div class="language__rus">--}}
            {{--                        <img src="img/rus.png" alt="">--}}
            {{--                        <span>Русский</span>--}}
            {{--                    </div>--}}
            {{--                    <div class="language__kz">--}}
            {{--                        <img src="img/kz.png" alt="">--}}
            {{--                        <span>Казахский</span>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            <p>© @lang('main.reserve')</p>
            <a href="#">@lang('main.policy')</a>
        </div>
    </div>
    </div>
    <div class="social">
        <div class="main-icon">
            <img src="{{ asset('img/icons/message.svg') }}" alt="">
            <img src="{{ asset('img/icons/icon-leaf.svg') }}" alt="">
        </div>
        <div class="second-icons">
            <a href="https://t.me/itparkalmaty" class="social__telegram">
                <img src="{{ asset('img/icons/telegram.svg') }}" alt="Telegram">
            </a>
            <a href="https://www.instagram.com/itparkalmaty/" class="social__insta">
                <img src="{{ asset('img/icons/instagram-2.svg') }}" alt="Instagram">
            </a>
            <a href="https://api.whatsapp.com/send?phone=++77473233032&amp;text=Здравствуйте, меня интересует разработка веб сайта." class="social__wp">
                <img src="{{ asset('img/icons/wp.svg') }}" alt="WhatsApp">
            </a>
        </div>
    </div>
</footer>
<script src="{{ mix('/js/all.js') }}"></script>

</body>
</html>
