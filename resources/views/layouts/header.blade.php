<!DOCTYPE html>
@if(session('locale') == 'ru' || session('locale') == null)
    <html lang="ru">
    @elseif(session('locale') == 'en')
        <html lang="en">
        @endif

        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport"
                  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <link rel="stylesheet" href="{{ mix('/css/all.css') }}">
            <link rel="stylesheet" id="theme-link">
            <title>{{ $title }}</title>
            <meta name="description" content="{{ $descrips }}">
            {{-- Карта --}}
            {{-- <script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full&lazy=true&skin=dark"></script> --}}
            <script type="text/javascript">
                var map;

                DG.then(function () {
                    map = DG.map('map', {
                        center: [43.23225, 76.94524],
                        zoom: 15
                    });
                    DG.marker([43.23225, 76.94524]).addTo(map).bindPopup('IT-Park')
                        .on('click', function (e) {
                            map.setView([e.latlng.lat, e.latlng.lng]);
                            // map.setMaxZoom(maxZoom);
                        });
                });
            </script>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=G-S8PMJ2BL3W"></script>
            <script>
                window.dataLayer = window.dataLayer || [];

                function gtag() {
                    dataLayer.push(arguments);
                }

                gtag('js', new Date());

                gtag('config', 'G-S8PMJ2BL3W');
            </script>

            <!-- Global site tag (gtag.js) - Google Ads: 392421005 -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=AW-392421005"></script>
            <script>
                window.dataLayer = window.dataLayer || [];

                function gtag() {
                    dataLayer.push(arguments);
                }

                gtag('js', new Date());

                gtag('config', 'AW-392421005');
            </script>

            <!-- Event snippet for Website traffic conversion page
        In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
            <script>
                function gtag_report_conversion(url) {
                    var callback = function () {
                        if (typeof(url) != 'undefined') {
                            window.location = url;
                        }
                    };
                    gtag('event', 'conversion', {
                        'send_to': 'AW-392421005/rXhFCKKls8QCEI29j7sB',
                        'event_callback': callback
                    });
                    return false;
                }
            </script>

            <!-- Facebook Pixel Code -->
            <script>
                !function(f,b,e,v,n,t,s)
                {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                    n.queue=[];t=b.createElement(e);t.async=!0;
                    t.src=v;s=b.getElementsByTagName(e)[0];
                    s.parentNode.insertBefore(t,s)}(window, document,'script',
                    'https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '493655728581098');
                fbq('track', 'PageView');
            </script>
            <noscript><img height="1" width="1" style="display:none"
                           src="https://www.facebook.com/tr?id=493655728581098&ev=PageView&noscript=1"
                /></noscript>
            <!-- End Facebook Pixel Code -->


            <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

            <!-- Yandex.Metrika counter -->
            <script type="text/javascript">
                (function (m, e, t, r, i, k, a) {
                    m[i] = m[i] || function () {
                        (m[i].a = m[i].a || []).push(arguments)
                    };
                    m[i].l = 1 * new Date();
                    k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
                })
                (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

                ym(75600958, "init", {
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true,
                    ecommerce: "dataLayer"
                });
            </script>
            <noscript>
                <div><img src="https://mc.yandex.ru/watch/75600958" style="position:absolute; left:-9999px;" alt=""/>
                </div>
            </noscript>
            <!-- /Yandex.Metrika counter -->

            <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
            <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
            <link rel="manifest" href="/site.webmanifest">
            <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
            <meta name="msapplication-TileColor" content="#132332">
            <meta name="theme-color" content="#132332">
            <meta name="msapplication-navbutton-color" content="#132332">
            <meta name="apple-mobile-web-app-capable" content="yes">
            <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        {{-- Open Graph  --}}

        </head>
        <body>
        <!-- NAVBAR -->
        <div class="navbar">
            <div class="container container-nav flex">
                <a href="{{ route('index') }}">
                    <div class="logo__img">
                        <img src="{{ asset('img/logo-circle.svg') }}" alt="IT Park" class="logo-circle bounce-5">
                        <img src="{{ asset('img/itpark.svg') }}" alt="IT Park" class="logo-text" id="logo-text">
                    </div>
                </a>
                <div class="nav__menu">
                    <nav>
                        <ul>
                            <li><a href="{{ route('portfolio') }}">@lang('main.portfolio')</a></li>
{{--                            <li><a href="{{ route('team') }}">@lang('main.team')</a></li>--}}
                            <li><a href="{{ route('services') }}">@lang('main.offers')</a></li>
                            <li><a href="{{ route('posts') }}">@lang('main.article')</a></li>
                        </ul>
                    </nav>
                    <div class="theme" id="theme-button">
                        <img src="img/icons/sun.svg" alt="">
                    </div>
                    <div class="language">
                        <div class="language__select">
                            <img @if(session('locale') == 'en') src="{{ asset('img/eng.png') }}"
                                 @elseif(session('locale') == 'ru' || session('locale') == null) src="{{ asset('img/rus.png') }}"
                                 @elseif(session('locale') == 'kz' || session('locale') == null) src="{{ asset('img/kz.png') }}"

                                 @endif alt="">
                            <span>@lang('main.lang')<span class="icon-arrow-down"></span>
                    </span>
                        </div>
                        <div class="language__list">
                            <a href="{{ route('locale', 'en') }}" class="language__eng">
                                <img src="img/eng.png" alt="">
                                <span>English</span>
                            </a>
                            <a href="{{ route('locale', 'ru') }}" class="language__rus">
                                <img src="img/rus.png" alt="">
                                <span>Русский</span>
                            </a>
                            {{--                    <a href="{{ route('locale', 'kz') }}" class="language__kz">--}}
                            {{--                        <img src="img/kz.png" alt="">--}}
                            {{--                        <span>Казахский</span>--}}
                            {{--                    </a>--}}
                        </div>
                    </div>
                    <div class="navbar__right">
                        <a href="tel:{{ $contacts->phone }}"><span class="icon-phone"></span> {{ $contacts->phone }}</a>
                        <a href="#" onclick="gtag_report_conversion()" class="btn btn-tel">@lang('main.call')</a>
                        <input type="hidden" id="lang" value="{{session('locale')}}">
                    </div>
                </div>
                <button class="hamburger hamburger--spring">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
                </button>
            </div>
        </div>
        <div class="loader-wrapper">
            <div class="leaves">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="loader">
            </div>
        </div>
