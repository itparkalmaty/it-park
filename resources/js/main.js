import Swiper from "swiper/bundle";
import "swiper/swiper-bundle.css";
import Swal from "sweetalert2";
document.addEventListener("DOMContentLoaded", function (event) {
    // Модалка
    const btnTel = document.querySelectorAll(".btn-tel"),
        btnOrder = document.querySelector(".btn-order");
    btnTel.forEach((item) => {
        item.addEventListener("click", (e) => {
            // let locale = document.getElementById('lang').value;
            console.log(locale);
            e.preventDefault();
            fbq("track", "SubmitApplication");
            Swal.fire({
                title: locale === "ru" ? "Заполните форму" : "Fill up the form",
                text:
                    locale === "ru"
                        ? "Выберите пожалуйста время, в какое мы можем Вам перезвонить"
                        : "",
                //   icon: 'error',
                buttonsStyling: false,
                customClass: {
                    confirmButton: "btn btn-order",
                    title: "modal-title",
                    content: "modal-descr",
                },
                confirmButtonText:
                    locale === "ru"
                        ? "Заказать обратный звонок"
                        : " Request a call",
                input: "date",
                width: 400,
                background: "#132332",
                showCloseButton: true,
                confirmButtonColor: "#1DB972",
                html: ` <div class="form__content">
        <p>${
            locale == "ru"
                ? "Выберите пожалуйста время,в какое мы можем Вам перезвонить"
                : ""
        }</p>
        <form action="/api/order" method="post" id="main_form">
        <div class="form__inputs">
        <input type="text" name="name" placeholder="${
            locale == "ru" ? "Полное имя" : "Full name"
        }" id="name">
        </div>
        <input type="tel" placeholder="${
            locale == "ru" ? "Ваш номер телефона" : "Your phone number"
        }" name="phone" id="phone" value="+7">
        <textarea name="comment" placeholder="${
            locale == "ru" ? "Комментарий" : "Comments"
        }" id="comment"></textarea>
        </div>
        </form>`,
            }).then(function (result) {
                if (result.isConfirmed) {
                    let dataObject = {
                        name: document.getElementById("name").value,
                        phone: document.getElementById("phone").value,
                        comment: document.getElementById("comment").value,
                    };

                    let data = [];
                    if (
                        dataObject.name === "" ||
                        dataObject.phone === "" ||
                        dataObject.phone.length < 11
                    ) {
                        if (locale === "ru") {
                            data.push("Пожалуйста, проверьте введённые данные");
                        } else if (locale === "en") {
                            data.push("Please, check your info");
                        }
                    }
                    // if (dataObject.phone.length < 11){
                    //     if (locale === 'ru') {
                    //         data.push("Пожалуйста, проверьте введённый телефонный номер")
                    //     }else if(locale === 'en'){
                    //         data.push("Please, check your phone number")
                    //     }
                    //
                    // }
                    console.log(dataObject.phone.length);
                    if (data.length) {
                        if (locale === "ru") {
                            Swal.fire({
                                title: "Ошибка!",
                                text: `${data.join(`. `)}`,
                                icon: "error",
                                background: "#132332",
                                buttonsStyling: false,
                                customClass: {
                                    confirmButton: "btn btn-order",
                                    title: "modal-title mb",
                                    content: "modal-descr",
                                },
                                confirmButtonText: "Хорошо",
                            });
                        } else if (locale === "en") {
                            Swal.fire({
                                title: "Error!",
                                text: `${data.join(`. `)}`,
                                icon: "error",
                                background: "#132332",
                                buttonsStyling: false,
                                customClass: {
                                    confirmButton: "btn btn-order",
                                    title: "modal-title mb",
                                    content: "modal-descr",
                                },
                                confirmButtonText: "OK",
                            });
                        }

                        return false;
                    }
                    console.log(dataObject);
                    fetch("/api/order", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                            // 'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: JSON.stringify(dataObject),
                    }).then((e) => {
                        if (e.status === 200) {
                            if (locale === "ru") {
                                Swal.fire({
                                    title: "Спасибо!",
                                    text: "Мы обязательно свяжемся с Вами в назначенное время",
                                    icon: "success",
                                    background: "#132332",
                                    buttonsStyling: false,
                                    customClass: {
                                        confirmButton: "btn btn-order",
                                        title: "modal-title mb",
                                        content: "modal-descr",
                                    },
                                    confirmButtonText: "Хорошо",
                                });
                            } else if (locale === "en") {
                                Swal.fire({
                                    title: "Thank you!",
                                    text: "We will contact you at the appointed time",
                                    icon: "success",
                                    background: "#132332",
                                    buttonsStyling: false,
                                    customClass: {
                                        confirmButton: "btn btn-order",
                                        title: "modal-title mb",
                                        content: "modal-descr",
                                    },
                                    confirmButtonText: "OK",
                                });
                            }
                        } else {
                            console.log(e);
                        }
                    });
                }
            });
        });
    });

    // btnOrder.addEventListener('click', (e) => {
    //   let target = e.target;
    //   if(target =)
    //   Swal.fire(
    //     'Мы скоро с вами свяжемся!',
    //     'You clicked the button!',
    //     'success'
    //   );
    // });
    // Анимация фона
    // const bg = document.querySelector('.main');
    // if (bg) {
    //     document.addEventListener("mousemove", function (e) {
    //         MoveBackground(e);
    //     });

    //     function MoveBackground(e) {
    //         //Рассчитываем, насколько далеко от начала оси находится курсор: 0 - 0, 0.5 - середина экрана, 1 - ширина экрана (например, 1920)
    //         //Затем умножаем получившееся число на 30 - настолько будет сдвигаться фон
    //         //Например, если курсор находится посередине страницы (0.5), то при умножении получится 15
    //         //Далее отнимаем половину от 30, чтобы фон мог двигаться как влево, так и вправо
    //         let offsetX = (e.clientX / window.innerWidth * 30) - 15;
    //         let offsetY = (e.clientY / window.innerHeight * 10) - 5;
    //         //Меняем положение фона
    //         bg.setAttribute("style", "background-position: " + offsetX + "px " + offsetY + "px;");
    //     }
    // }

    // Добавлять при скролле background
    function scrollHeader() {
        const nav = document.querySelector(".navbar");
        if (this.scrollY >= 200) nav.classList.add("navbar-scroll");
        else nav.classList.remove("navbar-scroll");
    }

    window.addEventListener("scroll", scrollHeader);
    // Слайдер отзывов
    const swiper = new Swiper(".swiper-container", {
        // Optional parameters
        direction: "vertical",
        // loop: true,
        slidesPerView: 1,
        slidesPerGroup: 1,
        // If we need pagination
        pagination: {
            el: ".swiper-pagination",
            type: "bullets",
        },
        breakpoints: {
            320: {
                direction: "horizontal",
            },

            700: {
                direction: "vertical",
            },
        },
        // Navigation arrows
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        // And if we need scrollbar
        scrollbar: {
            el: ".swiper-scrollbar",
        },
    });
    // Слайдер клиентов
    const clientsSlider = new Swiper(".clients-container", {
        // Optional parameters
        direction: "horizontal",
        loop: true,
        speed: 15000,
        slidesPerView: 6,
        slidesPerGroup: 6,
        spaceBetween: 30,
        // autoHeight: true,
        // If we need pagination
        pagination: {
            el: ".clients-pagination",
            type: "bullets",
        },
        // Navigation arrows
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        breakpoints: {
            320: {
                slidesPerView: 2,
                slidesPerGroup: 2,
                spaceBetween: 30,
            },
            720: {
                slidesPerView: 2,
                slidesPerGroup: 2,
            },
            1000: {
                slidesPerView: 6,
                slidesPerGroup: 6,
            },
        },
        // And if we need scrollbar
        scrollbar: {
            el: ".swiper-scrollbar",
        },
        autoplay: {
            delay: 0,
            disableOnInteraction: false,
        },
    });
    // //   Кликабельность карточек
    // const cardMain = document.querySelectorAll('.card');
    // if (cardMain) {
    //     cardMain.forEach(item => {
    //         item.addEventListener('click', () => {
    //             item.classList.toggle('active');
    //         });
    //     });
    // }
    // Слайдер логотипов
    const swiper3 = new Swiper(".swiper-cool", {
        // Optional parameters
        direction: "horizontal",
        loop: false,
        slidesPerView: 2,
        spaceBetween: 30,
        slidesPerGroup: 2,
        // centerSlides: true,
        // centerSlidesBounds: false,
        // If we need pagination
        pagination: {
            el: ".swiper-pagination",
        },
        // Navigation arrows
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        // And if we need scrollbar
        scrollbar: {
            el: ".swiper-scrollbar",
        },
    });
    // Слайдер работ
    const swiper4 = new Swiper(".works-container", {
        // Optional parameters
        direction: "horizontal",
        loop: false,
        slidesPerView: 1,
        spaceBetween: 30,
        slidesPerGroup: 1,
        // centerSlides: true,
        // centerSlidesBounds: false,
        // If we need pagination
        pagination: {
            el: ".swiper-pagination",
        },
        // Navigation arrows
        navigation: {
            nextEl: ".next-work",
            prevEl: ".swiper-button-prev",
        },
        // And if we need scrollbar
        scrollbar: {
            el: ".swiper-scrollbar",
        },
    });
    // Команда табы
    const teamTabs = document.querySelector(".team-people"),
        teamTab = document.querySelectorAll(".people-tab"),
        // teamPhotos = document.querySelectorAll('.people-tab img'),
        // teamPhoto = document.querySelector('.team-main'),
        teamContent = document.querySelectorAll(".team-personal");
    const hideTabContent = () => {
        if (teamContent) {
            teamContent.forEach((item) => {
                item.style.display = "none";
            });
            teamTab.forEach((item) => {
                item.classList.remove("people-active");
            });
        }
    };
    const showTabContent = (i = 0) => {
        if (teamContent[i] != undefined) {
            teamContent[i].style.display = "block";
            teamTab[i].classList.add("people-active");
        }
    };
    hideTabContent();
    showTabContent();
    if (teamTabs) {
        teamTabs.addEventListener("click", (e) => {
            const target = e.target;
            if (target && target.classList.contains("people-tab")) {
                teamTab.forEach((item, i) => {
                    if (target == item) {
                        hideTabContent();
                        showTabContent(i);
                    }
                });
            }
        });
    }
    const productImg = document.querySelector(".team-main"),
        smallImg = document.querySelectorAll(".people-tab");
    smallImg.forEach((item) => {
        item.addEventListener("click", () => {
            productImg.src = item.src;
            item.classList.add("people-active");
        });
    });
    // Табы услуг
    const serviceItem = document.querySelectorAll(".service-tab"),
        serviceItems = document.querySelector(".service-tabs"),
        serviceContent = document.querySelectorAll(".service-content");
    const hideServiceContent = () => {
        serviceContent.forEach((item) => {
            item.style.display = "none";
        });
        serviceItem.forEach((item) => {
            item.classList.remove("tab-active");
        });
    };
    const showServiceContent = (i = 0) => {
        if (serviceItem[i] != undefined) {
            serviceItem[i].classList.add("tab-active");
            serviceContent[i].style.display = "block";
        }
    };
    hideServiceContent();
    showServiceContent();

    if (serviceItems != undefined) {
        serviceItems.addEventListener("click", (e) => {
            const target = e.target;
            if (target && target.classList.contains("service-tab")) {
                serviceItem.forEach((item, i) => {
                    if (target == item) {
                        hideServiceContent();
                        showServiceContent(i);
                    }
                });
            }
        });
    }
});

// Табы услуг 2
const serviceTabItem = document.querySelectorAll(".service-tab-item"),
    serviceTabItems = document.querySelector(".service-tab-items"),
    serviceTabContent = document.querySelectorAll(".service-tab-content");
const hideServiceContent = () => {
    serviceTabContent.forEach((item) => {
        item.style.display = "none";
    });
    serviceTabItem.forEach((item) => {
        item.classList.remove("tab-item-active");
    });
};
const showServiceContent = (i = 0) => {
    if (serviceTabItem[i] != undefined) {
        serviceTabItem[i].classList.add("tab-item-active");
        serviceTabContent[i].style.display = "block";
    }
};
hideServiceContent();
showServiceContent();

if (serviceTabItems != undefined) {
    serviceTabItems.addEventListener("click", (e) => {
        const target = e.target;
        if (target && target.classList.contains("service-tab-item")) {
            serviceTabItem.forEach((item, i) => {
                if (target == item) {
                    hideServiceContent();
                    showServiceContent(i);
                }
            });
        }
    });
}
// Гамбургер скрипт
const hamburger = document.querySelector(".hamburger"),
    mobileMenu = document.querySelector(".nav__menu"),
    mainBg = document.querySelector(".main");
hamburger.addEventListener("click", () => {
    hamburger.classList.toggle("is-active");
    mobileMenu.classList.toggle("nav-active");
    if (mainBg != undefined) {
        mainBg.classList.toggle("content-move");
    }
    document.querySelector(".content").classList.toggle("content-move");
    document.querySelector("footer").classList.toggle("content-move");
    document.body.classList.toggle("overflow-hidden");
});

// Анимация преимуществ
const cards = document.querySelectorAll(".card");

if (cards.length > 0) {
    window.addEventListener("scroll", animScroll);
    function animScroll() {
        for (let index = 0; index < cards.length; index++) {
            const card = cards[index],
                cardHeight = card.offsetHeight,
                cardOffset = offset(card).top,
                animStart = 1.5;

            let animCardPoint = window.innerHeight - cardHeight / animStart;

            if (cardHeight > window.innerHeight) {
                window.innerHeight - window.innerHeight / animStart;
            }

            if (
                pageYOffset > cardOffset - animCardPoint &&
                pageYOffset < cardOffset + cardHeight
            ) {
                card.classList.add("active");
            } else {
                card.classList.remove("active");
            }
        }
        function offset(el) {
            const rect = el.getBoundingClientRect(),
                scrollLeft =
                    window.pageXOffset || document.documentElement.scrollLeft,
                scrollTop =
                    window.pageYOffset || document.documentElement.scrollTop;

            return { top: rect.top + scrollTop, left: rect.left + screenLeft };
        }
    }
}
setTimeout(() => {
    animScroll();
}, 600);

// Плавная прокрутка в моб версии
//   const sections = [...document.getElementsByTagName('section')];
//   let currentSection = 0;

//   window.addEventListener('wheel', function(e) {
//     e.preventDefault();

//     (e.deltaY < 0) ? --currentSection: ++currentSection;

//     if (currentSection < 0) currentSection = 0;
//     else if (currentSection > (sections.length - 1)) currentSection = (sections.length - 1);

//     scrollToSection(currentSection);
//   });

//   function scrollToSection(i) {
//     document.getElementById(sections[i].id).scrollIntoView({
//       behavior: 'smooth'
//     });
//   }

// window.scrollTo({ top: 900, behavior: 'smooth' })

// Загрузка страницы
const loader = document.querySelector(".loader-wrapper");

window.addEventListener("load", () => {
    loader.style.opacity = "0%";
    // loader.style.display = 'none';

    setTimeout(() => {
        loader.remove();
    }, 600);
});

// Переключатель на темную тему
const btnTheme = document.getElementById("theme-button");
const link = document.getElementById("theme-link");
const logoText = document.getElementById("logo-text");
const logoCircle = document.querySelector(".logo-circle");
const logoFooter = document.querySelector(".footer-logo");
let lightTheme = "css/white.css";
let darkTheme = "css/all.css";
let currTheme = link.getAttribute("href");
let setLightMode = localStorage.getItem("light");
let icon = (document.querySelector(".theme img").src = "img/icons/sun.svg");
btnTheme.addEventListener("click", function () {
    ChangeTheme();
});
function ChangeTheme() {
    setLightMode = localStorage.getItem("light");
    if (setLightMode !== "on") {
        lightMode();
        setLightMode = localStorage.setItem("light", "on");
    } else {
        darkMode();
        setLightMode = localStorage.setItem("light", null);
    }
}

function lightMode() {
    currTheme = lightTheme;
    // theme = "light";
    logoText.src = "img/park-black.svg";
    logoCircle.src = "img/logo-bg.svg";
    logoFooter.src = "img/logo-black.svg";
    link.setAttribute("href", lightTheme);
    let icon = document.querySelector(".theme img");
    icon.src = "img/icons/moon.svg";
    icon.style.cssText = "filter: none;";
}

function darkMode() {
    currTheme = darkTheme;
    //  theme = "dark";
    logoText.src = "img/itpark.svg";
    logoCircle.src = "img/logo-circle.svg";
    logoFooter.src = "img/footer-logo.png";
    link.setAttribute("href", darkTheme);
    let icon = document.querySelector(".theme img");
    icon.src = "img/icons/sun.svg";
    icon.style.cssText = "filter: invert(1);";
}

if (setLightMode === "on") {
    lightMode();
    // icon.style.cssText = 'filter: none;';
}
// // Ошибка статей
// const article = document.querySelectorAll('.article__card');

// article.forEach(item => {
//     item.addEventListener('click', (e) => {
//         e.preventDefault();
//         Swal.fire({
//             title: 'Извините!',
//             text: 'Раздел статей находится в разработке.',
//               icon: 'error',
//             buttonsStyling: false,
//             customClass: {
//                 confirmButton: 'btn btn-order',
//                 title: 'modal-title',
//                 content: 'modal-descr'
//             },
//             confirmButtonText: 'Жаль',
//             width: 400,
//             background: '#132332',
//             showCloseButton: true,
//             confirmButtonColor: '#1DB972',
//         });
//     });
// })

// Иконка соц сетей в футере
const mainIcon = document.querySelector(".main-icon"),
    socIcons = document.querySelector(".second-icons");
mainIcon.addEventListener("click", (event) => {
    socIcons.classList.toggle("visible");
    mainIcon.classList.toggle("filter-brightness");
});

let locale = document.documentElement.lang;

// Слайдер в портфолио
const portfolioSlider = new Swiper(".portfolio-sliders", {
    // Optional parameters
    direction: "horizontal",
    // loop: true,
    slidesPerView: 1,
    slidesPerGroup: 1,
    // If we need pagination
    pagination: {
        el: ".swiper-pagination",
        type: "bullets",
    },
    // Navigation arrows
    navigation: {
        nextEl: ".portfolio-slider-next",
        prevEl: ".portfolio-slider-back",
    },
    // And if we need scrollbar
    scrollbar: {
        el: ".swiper-scrollbar",
    },
});
