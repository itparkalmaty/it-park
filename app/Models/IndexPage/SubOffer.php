<?php

namespace App\Models\IndexPage;

use App\Models\Guarantee;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class SubOffer extends Model
{
    use HasFactory, Translatable;

    protected $translatable = ['title', 'description'];

    public function guarantee()
    {
        return $this->hasMany(Guarantee::class, 'sub_offer_id','id');
    }
}
