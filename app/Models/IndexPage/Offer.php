<?php

namespace App\Models\IndexPage;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Offer extends Model
{
    use HasFactory, Sluggable, Translatable;

    protected $translatable = ['title', 'description'];

    public function sluggable()
    {
        return[
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function subOffer()
    {
        return $this->hasMany(SubOffer::class, 'offer_id', 'id');
    }
}
