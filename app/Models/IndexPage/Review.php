<?php

namespace App\Models\IndexPage;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Review extends Model
{
    use HasFactory, Sluggable, Translatable;

    protected $translatable = ['review'];

    public function sluggable()
    {
        return[
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
