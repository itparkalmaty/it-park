<?php

namespace App\Models\IndexPage;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Advantages extends Model
{
    use HasFactory, Sluggable;

    public function sluggable()
    {
        return[
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
