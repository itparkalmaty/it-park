<?php

namespace App\Models\Portfolio;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

class ProjectType extends Model
{
    use HasFactory, SoftDeletes, Sluggable, Translatable;

    protected $translatable = ['title'];

    public function project()
    {
        return $this->hasOne(Project::class);
    }

    public function sluggable()
    {
        return[
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
