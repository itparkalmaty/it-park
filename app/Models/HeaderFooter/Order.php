<?php

namespace App\Models\HeaderFooter;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'phone',
        'comment'
    ];
}
