<?php

namespace App\Models\HeaderFooter;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

class Contact extends Model
{
    use HasFactory, SoftDeletes, Translatable;

    protected $translatable = ['address'];

}
