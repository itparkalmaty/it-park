<?php

namespace App\Models\CompanyInfo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Crew extends Model
{
    use HasFactory, Translatable;

    protected $translatable = ['description'];
}
