-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Июн 17 2021 г., 11:06
-- Версия сервера: 10.2.38-MariaDB-cll-lve
-- Версия PHP: 7.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `p-319221_it_park`
--

-- --------------------------------------------------------

--
-- Структура таблицы `advantages`
--

CREATE TABLE `advantages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(2, NULL, 1, 'Category 2', 'category-2', '2021-03-30 05:37:10', '2021-03-30 05:37:10');

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`id`, `image`, `name`, `created_at`, `updated_at`) VALUES
(1, 'clients/April2021/07BZuAd7NkRRvmf3RGUj.png', 'FedEx', '2021-04-04 15:02:30', '2021-04-07 05:32:35'),
(3, 'clients/April2021/pz73IWp6iNUOgsJFPMBR.png', 'MasterCard', '2021-04-04 15:03:07', '2021-04-07 05:14:46'),
(4, 'clients/April2021/iGGayH7XIlHWJDLWAZy2.png', 'rr', '2021-04-04 15:03:28', '2021-04-07 05:32:58'),
(6, 'clients/April2021/cFERpBtGyp2hfXO7x1OS.png', 'aa', '2021-04-07 02:18:58', '2021-04-07 05:34:42'),
(7, 'clients/April2021/ovhHwH7Q8ICYLLOaPtSF.png', '//', '2021-04-07 05:35:08', '2021-04-07 05:45:57'),
(8, 'clients/April2021/hqSV67UBZ1cIYjFuHPH5.png', '//', '2021-04-07 05:35:40', '2021-04-07 05:45:45');

-- --------------------------------------------------------

--
-- Структура таблицы `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `contacts`
--

INSERT INTO `contacts` (`id`, `address`, `phone`, `email`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Казахстан, Алматы, проспект Аль-Фараби 13к2в, 9 этаж, 903 офис', '+7 747 323 3032', 'info@itpark.com', NULL, '2021-04-02 05:36:46', '2021-04-28 03:36:13');

-- --------------------------------------------------------

--
-- Структура таблицы `contents`
--

CREATE TABLE `contents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `crews`
--

CREATE TABLE `crews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `iamge` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `crews`
--

INSERT INTO `crews` (`id`, `name`, `position`, `experience`, `description`, `created_at`, `updated_at`, `iamge`) VALUES
(5, 'Еркебулан', 'Маркетолог', '3 года', 'Лучшая работа — это высокооплачиваемое хобби.', '2021-04-26 23:44:00', '2021-04-27 03:54:12', 'crews/April2021/rOUAVOBOkKiVkjh0PxqE.jpg'),
(6, 'Алдияр', 'Backend-разработчик', '2 года', 'Дисциплина и труд - залоги успеха любой работы', '2021-04-27 01:56:00', '2021-04-27 03:49:37', 'crews/April2021/wQepBQrTN9OzSMgbgZiq.jpeg'),
(7, 'Асыл', 'Fullstack-разработчик', '3 года', 'Для меня разработка - это некий стиль жизни. Когда дело доходит до работы, я теряю счет времени и полностью погружаюсь в \"коде\". ', '2021-04-27 03:25:00', '2021-04-27 03:41:13', 'crews/April2021/nwKaPm3YKdddGXYB6tjh.jpeg'),
(8, 'Владимир', 'Frontend-разработчик', '2 года', 'Программирование всегда было моим методом самовыражения. Гармония кодов и дизайна - это то, что действительно разжигает во мне желание развиваться и идти вперед.', '2021-04-27 03:37:19', '2021-04-27 03:37:19', 'crews/April2021/6bsj2CvyszMwA0oUTETU.jpeg'),
(9, 'Александр', 'Операционный директор', '3 года', 'Слаженность команды и позитивный настрой являются моей главной обязанностью ', '2021-04-27 03:53:00', '2021-04-27 03:53:15', 'crews/April2021/nEoUUSreDIEBUBLBve0p.jpeg');

-- --------------------------------------------------------

--
-- Структура таблицы `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, '{}', 2),
(31, 5, 'category_id', 'text', 'Category', 0, 0, 1, 1, 1, 0, '{}', 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 0, 0, 1, 1, 1, 1, '{}', 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '{}', 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"allow_crop\":true,\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 0, 0, 1, 1, 1, 1, '{}', 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 0, 0, 1, 1, 1, 1, '{}', 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, '{}', 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, '{}', 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(56, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(57, 7, 'title', 'text', 'То, чем занимается компания', 1, 1, 1, 1, 1, 1, '{}', 2),
(58, 7, 'slug', 'text', 'Slug', 1, 1, 1, 0, 0, 0, '{}', 3),
(59, 7, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 4),
(60, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(61, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(62, 8, 'offer_id', 'text', 'Offer Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(63, 8, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 3),
(64, 8, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 4),
(65, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(68, 7, 'offer_hasmany_sub_offer_relationship', 'relationship', 'Вид услуги', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\IndexPage\\\\SubOffer\",\"table\":\"sub_offers\",\"type\":\"hasMany\",\"column\":\"offer_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"advantages\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(69, 8, 'sub_offer_belongsto_offer_relationship', 'relationship', 'offers', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\IndexPage\\\\Offer\",\"table\":\"offers\",\"type\":\"belongsTo\",\"column\":\"offer_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"advantages\",\"pivot\":\"0\",\"taggable\":\"0\"}', 6),
(70, 5, 'is_popular', 'checkbox', 'Is Popular', 1, 1, 1, 1, 1, 1, '{}', 16),
(71, 5, 'main_page', 'checkbox', 'Main Page', 1, 1, 1, 1, 1, 1, '{}', 17),
(72, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(73, 9, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(74, 9, 'slug', 'text', 'Slug', 1, 1, 1, 0, 0, 0, '{}', 3),
(75, 9, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 4),
(76, 9, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 5),
(77, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(78, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(79, 10, 'project_type_id', 'text', 'Project Type Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(80, 10, 'title', 'text', 'Название проекта', 1, 1, 1, 1, 1, 1, '{}', 3),
(81, 10, 'slug', 'text', 'Slug', 1, 1, 1, 0, 0, 0, '{}', 4),
(82, 10, 'image', 'image', 'Картинка', 1, 1, 1, 1, 1, 1, '{}', 5),
(83, 10, 'description', 'text_area', 'Описание проекта', 1, 1, 1, 1, 1, 1, '{}', 6),
(84, 10, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 7),
(85, 10, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 8),
(86, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(87, 10, 'project_belongsto_project_type_relationship', 'relationship', 'project_types', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Portfolio\\\\ProjectType\",\"table\":\"project_types\",\"type\":\"belongsTo\",\"column\":\"project_type_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"advantages\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(88, 7, 'order', 'text', 'Порядок', 0, 1, 1, 1, 1, 1, '{}', 6),
(89, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(90, 11, 'image', 'image', 'Картинка', 1, 1, 1, 1, 1, 1, '{}', 2),
(91, 11, 'name', 'text', 'Наименование компании', 1, 1, 1, 1, 1, 1, '{}', 3),
(92, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 4),
(93, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(94, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(95, 12, 'stars', 'text', 'Звезды', 1, 1, 1, 1, 1, 1, '{}', 2),
(96, 12, 'title', 'text', 'Наименование компании', 1, 1, 1, 1, 1, 1, '{}', 3),
(97, 12, 'slug', 'text', 'Slug', 1, 1, 1, 0, 0, 0, '{}', 4),
(98, 12, 'review', 'text_area', 'Отзыв', 1, 1, 1, 1, 1, 1, '{}', 5),
(99, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(100, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(106, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(107, 15, 'address', 'text', 'Адрес', 1, 1, 1, 1, 1, 1, '{}', 2),
(108, 15, 'phone', 'text', 'Телефон', 1, 1, 1, 1, 1, 1, '{}', 3),
(109, 15, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 4),
(110, 15, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 5),
(111, 15, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 6),
(112, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(113, 12, 'image', 'image', 'Картинка', 1, 1, 1, 1, 1, 1, '{}', 8),
(114, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(115, 16, 'name', 'text', 'Имя', 1, 1, 1, 1, 1, 1, '{}', 2),
(116, 16, 'position', 'text', 'Должность', 1, 1, 1, 1, 1, 1, '{}', 3),
(117, 16, 'experience', 'text', 'Опыт работы', 1, 1, 1, 1, 1, 1, '{}', 4),
(118, 16, 'description', 'text_area', 'О себе', 1, 1, 1, 1, 1, 1, '{}', 5),
(119, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(120, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(121, 16, 'iamge', 'image', 'Iamge', 1, 1, 1, 1, 1, 1, '{}', 8),
(122, 7, 'description', 'text_area', 'Описание услуги', 1, 1, 1, 1, 1, 1, '{}', 7),
(125, 8, 'price', 'text', 'Стоимость', 1, 1, 1, 1, 1, 1, '{}', 6),
(126, 8, 'best_offer', 'checkbox', 'Лучшее предложение', 1, 1, 1, 1, 1, 1, '{}', 7),
(127, 8, 'description', 'text_area', 'Описание', 0, 1, 1, 1, 1, 1, '{}', 8),
(128, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(129, 17, 'name', 'text', 'Имя заказчика', 0, 1, 1, 1, 1, 1, '{}', 2),
(130, 17, 'phone', 'text', 'Номер телефона', 0, 1, 1, 1, 1, 1, '{}', 3),
(131, 17, 'comment', 'text_area', 'Комментарии', 0, 1, 1, 1, 1, 1, '{}', 4),
(132, 17, 'created_at', 'timestamp', 'Заявка оставлена', 0, 1, 1, 0, 0, 0, '{}', 5),
(133, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(134, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(135, 18, 'sub_offer_id', 'text', 'Sub Offer Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(136, 18, 'title', 'text', 'Что по гарантируется', 1, 1, 1, 1, 1, 1, '{}', 3),
(137, 18, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 4),
(138, 18, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 5),
(139, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(140, 18, 'guarantee_belongsto_sub_offer_relationship', 'relationship', 'sub_offers', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\IndexPage\\\\SubOffer\",\"table\":\"sub_offers\",\"type\":\"belongsTo\",\"column\":\"sub_offer_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"advantages\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(141, 8, 'sub_offer_hasmany_guarantee_relationship', 'relationship', 'guarantees', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Guarantee\",\"table\":\"guarantees\",\"type\":\"hasMany\",\"column\":\"sub_offer_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"advantages\",\"pivot\":\"0\",\"taggable\":null}', 9);

-- --------------------------------------------------------

--
-- Структура таблицы `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2021-03-30 05:37:10', '2021-04-28 00:51:30'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(7, 'offers', 'offers', 'Offer', 'Offers', NULL, 'App\\Models\\IndexPage\\Offer', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-03-30 06:13:27', '2021-04-05 01:19:07'),
(8, 'sub_offers', 'sub-offers', 'Sub Offer', 'Sub Offers', NULL, 'App\\Models\\IndexPage\\SubOffer', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-03-30 06:15:37', '2021-04-05 02:32:56'),
(9, 'project_types', 'project-types', 'Project Type', 'Project Types', NULL, 'App\\Models\\Portfolio\\ProjectType', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-03-31 02:12:36', '2021-03-31 02:12:36'),
(10, 'projects', 'projects', 'Проект', 'Проекты', NULL, 'App\\Models\\Portfolio\\Project', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-03-31 02:19:06', '2021-04-04 10:23:55'),
(11, 'clients', 'clients', 'Клиенты', 'Наши клиенты', NULL, 'App\\Models\\IndexPage\\Client', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-02 02:24:06', '2021-04-04 15:01:46'),
(12, 'reviews', 'reviews', 'Отзыв', 'Отзывы', NULL, 'App\\Models\\IndexPage\\Review', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-02 02:24:10', '2021-04-04 15:15:59'),
(15, 'contacts', 'contacts', 'Контакты', 'Контакты', NULL, 'App\\Models\\HeaderFooter\\Contact', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-02 05:32:15', '2021-04-02 05:32:15'),
(16, 'crews', 'crews', 'Команда', 'Команда', NULL, 'App\\Models\\CompanyInfo\\Crew', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-04-04 16:07:26', '2021-04-04 16:07:26'),
(17, 'orders', 'orders', 'Заявка на услугу', 'Заявки на услуги', NULL, 'App\\Models\\HeaderFooter\\Order', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-06 04:43:23', '2021-04-06 04:45:41'),
(18, 'guarantees', 'guarantees', 'Гарантия', 'Гарантии', NULL, 'App\\Models\\Guarantee', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-04-06 05:50:49', '2021-04-06 05:59:07');

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `guarantees`
--

CREATE TABLE `guarantees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sub_offer_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `guarantees`
--

INSERT INTO `guarantees` (`id`, `sub_offer_id`, `title`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 9, 'Таргетированная реклама', NULL, '2021-04-06 05:56:11', '2021-04-06 06:14:19'),
(2, 9, 'Контекстная реклама', NULL, '2021-04-06 06:00:27', '2021-04-06 06:00:27'),
(3, 9, 'Ведение социальных сетей', NULL, '2021-04-06 06:14:37', '2021-04-06 06:14:37'),
(4, 24, 'Дизайн', NULL, '2021-04-06 06:16:35', '2021-04-23 00:31:48'),
(5, 24, 'Вёрстка', NULL, '2021-04-06 06:16:56', '2021-04-23 00:31:02'),
(6, 24, 'Система управления', NULL, '2021-04-06 06:17:34', '2021-04-23 00:30:29'),
(7, 24, 'Домен и хостинг', NULL, '2021-04-06 06:18:32', '2021-04-23 00:29:02');

-- --------------------------------------------------------

--
-- Структура таблицы `infos`
--

CREATE TABLE `infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `infos`
--

INSERT INTO `infos` (`id`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'infos\\April2021\\hcPYJ43R7Iom64oWpbPt.jpg', 'Наша Web-студия оказывает комплексный подход по развитию и продвижению вашего онлайн бизнеса. На первом этапе мы разрабатываем уникальный сайт с учетом всех пожеланий клиента, а после этого начинаем его активно продвигать в сети, используя современные инструменты и проверенные стратегии.', '2021-04-02 02:28:49', '2021-04-02 02:28:49');

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2021-03-30 05:37:09', '2021-03-30 05:37:09');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2021-03-30 05:37:09', '2021-03-30 05:37:09', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2021-03-30 05:37:09', '2021-03-30 05:37:09', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2021-03-30 05:37:09', '2021-03-30 05:37:09', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2021-03-30 05:37:09', '2021-03-30 05:37:09', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2021-03-30 05:37:09', '2021-03-30 05:37:09', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2021-03-30 05:37:09', '2021-03-30 05:37:09', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2021-03-30 05:37:09', '2021-03-30 05:37:09', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2021-03-30 05:37:09', '2021-03-30 05:37:09', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2021-03-30 05:37:09', '2021-03-30 05:37:09', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2021-03-30 05:37:09', '2021-03-30 05:37:09', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 8, '2021-03-30 05:37:10', '2021-03-30 05:37:10', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 6, '2021-03-30 05:37:10', '2021-03-30 05:37:10', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 7, '2021-03-30 05:37:10', '2021-03-30 05:37:10', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2021-03-30 05:37:10', '2021-03-30 05:37:10', 'voyager.hooks', NULL),
(15, 1, 'Offers', '', '_self', NULL, NULL, NULL, 15, '2021-03-30 06:13:27', '2021-03-30 06:13:27', 'voyager.offers.index', NULL),
(16, 1, 'Sub Offers', '', '_self', NULL, NULL, NULL, 16, '2021-03-30 06:15:38', '2021-03-30 06:15:38', 'voyager.sub-offers.index', NULL),
(17, 1, 'Project Types', '', '_self', NULL, NULL, NULL, 17, '2021-03-31 02:12:36', '2021-03-31 02:12:36', 'voyager.project-types.index', NULL),
(18, 1, 'Projects', '', '_self', NULL, NULL, NULL, 18, '2021-03-31 02:19:06', '2021-03-31 02:19:06', 'voyager.projects.index', NULL),
(19, 1, 'Наши клиенты', '', '_self', NULL, NULL, NULL, 19, '2021-04-02 02:24:06', '2021-04-02 02:24:06', 'voyager.clients.index', NULL),
(20, 1, 'Отзывы', '', '_self', NULL, NULL, NULL, 20, '2021-04-02 02:24:10', '2021-04-02 02:24:10', 'voyager.reviews.index', NULL),
(22, 1, 'Контакты', '', '_self', NULL, NULL, NULL, 22, '2021-04-02 05:32:15', '2021-04-02 05:32:15', 'voyager.contacts.index', NULL),
(23, 1, 'Команда', '', '_self', NULL, NULL, NULL, 23, '2021-04-04 16:07:26', '2021-04-04 16:07:26', 'voyager.crews.index', NULL),
(24, 1, 'Заявки на услуги', '', '_self', NULL, NULL, NULL, 24, '2021-04-06 04:43:23', '2021-04-06 04:43:23', 'voyager.orders.index', NULL),
(25, 1, 'Гарантии', '', '_self', NULL, NULL, NULL, 25, '2021-04-06 05:50:49', '2021-04-06 05:50:49', 'voyager.guarantees.index', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(24, '2021_03_30_062002_create_welcome_titles_table', 1),
(25, '2016_01_01_000000_create_pages_table', 2),
(26, '2016_01_01_000000_create_posts_table', 2),
(27, '2016_02_15_204651_create_categories_table', 2),
(28, '2017_04_11_000000_alter_post_nullable_fields_table', 2),
(29, '2021_03_30_115201_create_offers_table', 3),
(30, '2021_03_30_115621_create_sub_offers_table', 4),
(31, '2021_03_30_115700_create_infos_table', 4),
(32, '2021_03_30_115820_create_reviews_table', 4),
(33, '2021_03_30_115915_create_clients_table', 4),
(34, '2021_03_30_115955_create_advantages_table', 4),
(35, '2021_03_30_120700_add_is_popular_to_posts_table', 5),
(38, '2021_03_31_061718_create_project_types_table', 6),
(39, '2021_03_31_062700_create_projects_table', 6),
(46, '2021_03_31_083155_create_crews_table', 7),
(47, '2021_03_31_091017_create_contents_table', 7),
(48, '2021_03_31_102925_create_orders_table', 7),
(49, '2021_04_01_081823_create_contacts_table', 8),
(51, '2021_04_02_064846_add_order_to_offers_table', 9),
(52, '2021_04_04_211253_add_image_for_reviews_table', 10),
(53, '2021_04_04_220526_add_image_for_crews_table', 11),
(55, '2021_04_05_060723_add_description_to_offers', 12),
(57, '2021_04_05_071626_add_description_to_sub_offers', 13),
(58, '2021_04_06_113037_create_guarantees_table', 14);

-- --------------------------------------------------------

--
-- Структура таблицы `offers`
--

CREATE TABLE `offers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `offers`
--

INSERT INTO `offers` (`id`, `title`, `slug`, `created_at`, `updated_at`, `order`, `description`) VALUES
(1, 'Дизайн', 'dizayn', '2021-03-30 06:19:28', '2021-04-06 04:41:19', 1, 'Хотите дизайн не как у Цон\'а ? Тогда Вы по адресу. Создадим уникальный UI/UX дизайн.'),
(3, 'Программные обеспечения', 'razrabotka', '2021-03-31 00:09:48', '2021-04-06 04:42:33', 4, 'Разработаем бесперебойное программное обеспечение для оптимизации вашего бизнеса.'),
(4, 'Cайты', 'teksty-i-perevody', '2021-04-02 00:26:09', '2021-04-06 04:44:08', 2, 'Создание сайтов без перегрузок и багов, с корректной мобильной адаптацией.'),
(5, 'SMM', 'seo', '2021-04-02 00:41:48', '2021-04-23 00:20:01', 3, 'Увеличим поток клиентов для Вашего бренда.'),
(7, 'Мобильные приложения', 'poligrafiya-i-aydentika', '2021-04-02 00:45:34', '2021-04-06 04:52:38', 5, 'Реализуем любую идею перспективного стартапа');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `name`, `phone`, `comment`, `created_at`, `updated_at`) VALUES
(1, 'vova', '87014800350', 'ovavovavovavovoavoaovoavaovoavoaovoavoavo', '2021-04-05 23:25:33', '2021-04-05 23:25:33'),
(2, 'aldi test', '87014800350', 'rewtyrewq', '2021-04-06 01:05:20', '2021-04-06 01:05:20'),
(3, 'ONDCS', '6788765', 'ygtfgyscbsbdc', '2021-04-06 07:02:03', '2021-04-06 07:02:03'),
(4, 'Yerkebulan Toktabek', '+77771388732', 'dfdf', '2021-04-06 07:03:01', '2021-04-06 07:03:01'),
(5, 'Yerkebulan Toktabek', '+77771388732', NULL, '2021-04-06 07:03:13', '2021-04-06 07:03:13'),
(6, 'Александр', '+7 (747) 323-30-32', 'ТЕСТ ТЕСТ ТЕСТ', '2021-04-06 08:58:09', '2021-04-06 08:58:09'),
(7, 'aldi test', '87014800350', 'test mail', '2021-04-06 09:04:01', '2021-04-06 09:04:01'),
(8, 'Bek', '877 777 77 77', 'hsdfjshdbfushdf', '2021-04-06 09:06:20', '2021-04-06 09:06:20'),
(9, 'sigma', '97687', 'dknksjdsdf', '2021-04-06 09:07:51', '2021-04-06 09:07:51'),
(10, 'aldi test', '676545678', 'kjdfksjbdfksjdbf', '2021-04-06 09:10:32', '2021-04-06 09:10:32'),
(11, 'Алдияр', '9999999', 'hhjhjhjhjh', '2021-04-06 09:25:25', '2021-04-06 09:25:25'),
(12, 'asd', '98989', 'vgvgvgvg', '2021-04-06 09:32:55', '2021-04-06 09:32:55'),
(13, 'Aldiyar', '877 777 77 77', '567898765', '2021-04-06 09:41:09', '2021-04-06 09:41:09'),
(14, 'Aldiyar', '09i08098', 'ioijoahivhbddf', '2021-04-06 09:42:36', '2021-04-06 09:42:36'),
(15, 'aldi test', 'uioi9', 'bjdbhbsdcs', '2021-04-06 09:47:08', '2021-04-06 09:47:08'),
(16, '878787877', '878787877', '878787877', '2021-04-06 09:50:48', '2021-04-06 09:50:48'),
(17, '567uio', '567uio', '567uio567uio567uio567uio', '2021-04-06 09:54:35', '2021-04-06 09:54:35'),
(18, 'Aldiyar', '877 777 77 77', 'rytgyegrtyegrtert', '2021-04-06 09:56:17', '2021-04-06 09:56:17'),
(19, 'Aldiyar', '87014800350', '456890-=-098765', '2021-04-06 09:58:17', '2021-04-06 09:58:17'),
(20, 'Енесей', '8 888832589325902385', 'Как жизнь братане Тест', '2021-04-06 23:45:34', '2021-04-06 23:45:34'),
(21, 'Yerkebulan Toktabek', '+77771388732', 'ббб', '2021-04-07 01:25:03', '2021-04-07 01:25:03'),
(22, 'Bek', '+7 (701) 160-20-96', 'afsdfsdf', '2021-04-07 05:31:44', '2021-04-07 05:31:44'),
(23, 'Yerkebulan Toktabek', '+77771388732', ';;', '2021-04-09 04:38:11', '2021-04-09 04:38:11'),
(24, 'Абишева Кундыз', '+77717052662', 'Добрый день! Необходимо разработать интернет-магазин на базе битрикс 1С - на готовой платформе Аспро', '2021-04-12 22:47:04', '2021-04-12 22:47:04'),
(25, 'Петр', '+77772885576', 'Требуется создание сайта', '2021-04-14 05:30:55', '2021-04-14 05:30:55'),
(26, 'Валерия', '+7 705 189 03 56', 'Сайт натяжных потолков', '2021-04-18 20:02:20', '2021-04-18 20:02:20'),
(27, 'Рагиф', '+77783296666', 'Разработать мобильное приложение для Android & IOS', '2021-04-19 20:41:45', '2021-04-19 20:41:45'),
(28, 'Валерия', '+7 (705) 746-32-82', 'создание сайта каталога', '2021-04-20 22:23:56', '2021-04-20 22:23:56'),
(29, 'asyl', '+77073039917', 'test', '2021-04-21 04:34:47', '2021-04-21 04:34:47'),
(30, 'Ан Надира', '+77026360333', 'У нас производство металлических обородуваний. Сайт уже имеется, хотим усовершенствовать.', '2021-04-23 00:25:38', '2021-04-23 00:25:38'),
(31, 'Tina', '+17867784482', 'Напишите в ватсап', '2021-04-23 06:02:41', '2021-04-23 06:02:41'),
(32, 'Александр', '+77473233032', 'Проверка заказа услуги (тест)', '2021-04-26 21:48:10', '2021-04-26 21:48:10'),
(33, 'Тест', '+77473233032', 'Тест', '2021-04-26 21:49:42', '2021-04-26 21:49:42'),
(34, 'Тест', '+77473233032', 'Тест', '2021-04-26 21:50:24', '2021-04-26 21:50:24'),
(35, 'Рано', '+77001171716', 'Здравствуйте, интересует разработка сайта', '2021-04-28 20:54:52', '2021-04-28 20:54:52'),
(36, 'TEST', '+788888888888', 'hfsudhfusdf', '2021-04-29 00:40:29', '2021-04-29 00:40:29'),
(37, 'Александр', '+77477213502', NULL, '2021-04-29 23:13:52', '2021-04-29 23:13:52'),
(38, 'Ануар Алимжанович', '+7 (777) 666-64-83', NULL, '2021-05-03 01:09:17', '2021-05-03 01:09:17'),
(39, 'Адема', '+77027111005', NULL, '2021-05-03 23:07:10', '2021-05-03 23:07:10'),
(40, 'Daulet', '+7 (707) 52-70-299', NULL, '2021-05-16 08:20:31', '2021-05-16 08:20:31'),
(41, 'Игорь', '+77052395390', 'Разработка мобильной игры Android / iOS', '2021-05-18 05:34:11', '2021-05-18 05:34:11'),
(42, 'Агжан', '+77057773548', 'Нам нужно разработать визитку для наших юридических и консалтинговых услуг.  дизайн макет до трех вариантов, удобная система управления сайтом, модуль обратной связи и схема проезда, информационные блоки, отзывы, карта сайта, наполнение сайта до 5 страниц. Жду Ваше коммерческое предложение', '2021-05-19 00:56:04', '2021-05-19 00:56:04'),
(43, 'Айдана', '+77083744634', NULL, '2021-05-19 02:08:23', '2021-05-19 02:08:23'),
(44, 'Алия', '+7 (771) 999-03-88', 'напишите на вотсп, пожалуйста, я вам отправлю ТЗ', '2021-05-20 00:28:21', '2021-05-20 00:28:21'),
(45, 'TEST', '+777777777777777', 'ijdbfisbfisbdfoawubdfasdf', '2021-05-20 23:33:44', '2021-05-20 23:33:44'),
(46, 'Мелисса', '+77085080028', NULL, '2021-05-25 05:42:35', '2021-05-25 05:42:35'),
(47, 'Альбина', '+77021087040', 'Сайт для бухгалтерии с интеграцией 1с и личным кабинетом', '2021-05-28 02:38:16', '2021-05-28 02:38:16'),
(48, 'Юлия', '+77755576437', 'Интернет магазин', '2021-05-30 21:58:21', '2021-05-30 21:58:21'),
(49, 'Камила', '+7 (778) 546-6656', NULL, '2021-05-31 04:31:03', '2021-05-31 04:31:03'),
(50, 'Эльвира', '+77075570710', 'МОБИЛЬНОЕ ПРИЛОДЕНИЕ ДЛЯ КЛИЕНТОВ КАРГО, ОТСЛЕЖИВАНИЕ СВОИХ ПОСЫЛОК', '2021-06-01 22:46:00', '2021-06-01 22:46:00'),
(51, 'Nurlybek', '+7(702) 577-90-91', NULL, '2021-06-04 06:25:47', '2021-06-04 06:25:47'),
(52, 'София', '+77073669576', 'Добрый день. Мне необходимо мобильное приложение с возможностью онлайн-оплаты, заказа товара и доставкой (магазин).', '2021-06-09 04:53:23', '2021-06-09 04:53:23'),
(53, 'Арина', '+7 778 520 5006', 'Нужно приложение, для того чтоб в нем была бонусная карта магазина, количество бонусов в данный момент на карте и т.д.', '2021-06-13 23:00:00', '2021-06-13 23:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2021-03-30 05:37:10', '2021-03-30 05:37:10');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(2, 'browse_bread', NULL, '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(3, 'browse_database', NULL, '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(4, 'browse_media', NULL, '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(5, 'browse_compass', NULL, '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(6, 'browse_menus', 'menus', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(7, 'read_menus', 'menus', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(8, 'edit_menus', 'menus', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(9, 'add_menus', 'menus', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(10, 'delete_menus', 'menus', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(11, 'browse_roles', 'roles', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(12, 'read_roles', 'roles', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(13, 'edit_roles', 'roles', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(14, 'add_roles', 'roles', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(15, 'delete_roles', 'roles', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(16, 'browse_users', 'users', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(17, 'read_users', 'users', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(18, 'edit_users', 'users', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(19, 'add_users', 'users', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(20, 'delete_users', 'users', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(21, 'browse_settings', 'settings', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(22, 'read_settings', 'settings', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(23, 'edit_settings', 'settings', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(24, 'add_settings', 'settings', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(25, 'delete_settings', 'settings', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(26, 'browse_categories', 'categories', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(27, 'read_categories', 'categories', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(28, 'edit_categories', 'categories', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(29, 'add_categories', 'categories', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(30, 'delete_categories', 'categories', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(31, 'browse_posts', 'posts', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(32, 'read_posts', 'posts', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(33, 'edit_posts', 'posts', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(34, 'add_posts', 'posts', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(35, 'delete_posts', 'posts', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(36, 'browse_pages', 'pages', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(37, 'read_pages', 'pages', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(38, 'edit_pages', 'pages', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(39, 'add_pages', 'pages', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(40, 'delete_pages', 'pages', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(41, 'browse_hooks', NULL, '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(42, 'browse_offers', 'offers', '2021-03-30 06:13:27', '2021-03-30 06:13:27'),
(43, 'read_offers', 'offers', '2021-03-30 06:13:27', '2021-03-30 06:13:27'),
(44, 'edit_offers', 'offers', '2021-03-30 06:13:27', '2021-03-30 06:13:27'),
(45, 'add_offers', 'offers', '2021-03-30 06:13:27', '2021-03-30 06:13:27'),
(46, 'delete_offers', 'offers', '2021-03-30 06:13:27', '2021-03-30 06:13:27'),
(47, 'browse_sub_offers', 'sub_offers', '2021-03-30 06:15:38', '2021-03-30 06:15:38'),
(48, 'read_sub_offers', 'sub_offers', '2021-03-30 06:15:38', '2021-03-30 06:15:38'),
(49, 'edit_sub_offers', 'sub_offers', '2021-03-30 06:15:38', '2021-03-30 06:15:38'),
(50, 'add_sub_offers', 'sub_offers', '2021-03-30 06:15:38', '2021-03-30 06:15:38'),
(51, 'delete_sub_offers', 'sub_offers', '2021-03-30 06:15:38', '2021-03-30 06:15:38'),
(52, 'browse_project_types', 'project_types', '2021-03-31 02:12:36', '2021-03-31 02:12:36'),
(53, 'read_project_types', 'project_types', '2021-03-31 02:12:36', '2021-03-31 02:12:36'),
(54, 'edit_project_types', 'project_types', '2021-03-31 02:12:36', '2021-03-31 02:12:36'),
(55, 'add_project_types', 'project_types', '2021-03-31 02:12:36', '2021-03-31 02:12:36'),
(56, 'delete_project_types', 'project_types', '2021-03-31 02:12:36', '2021-03-31 02:12:36'),
(57, 'browse_projects', 'projects', '2021-03-31 02:19:06', '2021-03-31 02:19:06'),
(58, 'read_projects', 'projects', '2021-03-31 02:19:06', '2021-03-31 02:19:06'),
(59, 'edit_projects', 'projects', '2021-03-31 02:19:06', '2021-03-31 02:19:06'),
(60, 'add_projects', 'projects', '2021-03-31 02:19:06', '2021-03-31 02:19:06'),
(61, 'delete_projects', 'projects', '2021-03-31 02:19:06', '2021-03-31 02:19:06'),
(62, 'browse_clients', 'clients', '2021-04-02 02:24:06', '2021-04-02 02:24:06'),
(63, 'read_clients', 'clients', '2021-04-02 02:24:06', '2021-04-02 02:24:06'),
(64, 'edit_clients', 'clients', '2021-04-02 02:24:06', '2021-04-02 02:24:06'),
(65, 'add_clients', 'clients', '2021-04-02 02:24:06', '2021-04-02 02:24:06'),
(66, 'delete_clients', 'clients', '2021-04-02 02:24:06', '2021-04-02 02:24:06'),
(67, 'browse_reviews', 'reviews', '2021-04-02 02:24:10', '2021-04-02 02:24:10'),
(68, 'read_reviews', 'reviews', '2021-04-02 02:24:10', '2021-04-02 02:24:10'),
(69, 'edit_reviews', 'reviews', '2021-04-02 02:24:10', '2021-04-02 02:24:10'),
(70, 'add_reviews', 'reviews', '2021-04-02 02:24:10', '2021-04-02 02:24:10'),
(71, 'delete_reviews', 'reviews', '2021-04-02 02:24:10', '2021-04-02 02:24:10'),
(77, 'browse_contacts', 'contacts', '2021-04-02 05:32:15', '2021-04-02 05:32:15'),
(78, 'read_contacts', 'contacts', '2021-04-02 05:32:15', '2021-04-02 05:32:15'),
(79, 'edit_contacts', 'contacts', '2021-04-02 05:32:15', '2021-04-02 05:32:15'),
(80, 'add_contacts', 'contacts', '2021-04-02 05:32:15', '2021-04-02 05:32:15'),
(81, 'delete_contacts', 'contacts', '2021-04-02 05:32:15', '2021-04-02 05:32:15'),
(82, 'browse_crews', 'crews', '2021-04-04 16:07:26', '2021-04-04 16:07:26'),
(83, 'read_crews', 'crews', '2021-04-04 16:07:26', '2021-04-04 16:07:26'),
(84, 'edit_crews', 'crews', '2021-04-04 16:07:26', '2021-04-04 16:07:26'),
(85, 'add_crews', 'crews', '2021-04-04 16:07:26', '2021-04-04 16:07:26'),
(86, 'delete_crews', 'crews', '2021-04-04 16:07:26', '2021-04-04 16:07:26'),
(87, 'browse_orders', 'orders', '2021-04-06 04:43:23', '2021-04-06 04:43:23'),
(88, 'read_orders', 'orders', '2021-04-06 04:43:23', '2021-04-06 04:43:23'),
(89, 'edit_orders', 'orders', '2021-04-06 04:43:23', '2021-04-06 04:43:23'),
(90, 'add_orders', 'orders', '2021-04-06 04:43:23', '2021-04-06 04:43:23'),
(91, 'delete_orders', 'orders', '2021-04-06 04:43:23', '2021-04-06 04:43:23'),
(92, 'browse_guarantees', 'guarantees', '2021-04-06 05:50:49', '2021-04-06 05:50:49'),
(93, 'read_guarantees', 'guarantees', '2021-04-06 05:50:49', '2021-04-06 05:50:49'),
(94, 'edit_guarantees', 'guarantees', '2021-04-06 05:50:49', '2021-04-06 05:50:49'),
(95, 'add_guarantees', 'guarantees', '2021-04-06 05:50:49', '2021-04-06 05:50:49'),
(96, 'delete_guarantees', 'guarantees', '2021-04-06 05:50:49', '2021-04-06 05:50:49');

-- --------------------------------------------------------

--
-- Структура таблицы `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_popular` tinyint(1) NOT NULL DEFAULT 0,
  `main_page` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`, `is_popular`, `main_page`) VALUES
(1, 1, 1, 'Создание интернет-магазина', 'разработка сайта', 'Интернет-магазин является отличным инструментом повышения продаж для Вашего бренда. Так как..', '<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Вступление В современном мире торговля через Интернет стала неотъемлемой частью нашей жизни. Этот рынок уже хорошо развит не только на западе, но и у нас, в рунете, поэтому в особом представлении не нуждается. В 2010 году ожидается, что объем розничной интернет-торговли в рунете превысит 10 млрд. долларов и при этом сохранит высокие темпы роста в несколько десятков процентов, в то время как сайты данной тематики посещают несколько миллионов покупателей ежедневно, а около 70% посетителей совершают покупки (по данным &laquo;ROMIR&raquo;). Это уже очень значительные объемы, за которые борются многие известные компании из России, Украины и других русскоязычных стран. Сейчас в Интернете можно купить буквально все, от статьи за пару долларов до автомобиля за сто тысяч. Однако разные товары и услуги продаются с разным успехом, что связано с особенностями Интернета и особым поведением покупателей. Популярностью пользуются недорогие товары в ценовом диапазоне от 15$ до 350$ (по данным RUметрика): книги, бытовая техника, компьютеры, мобильные телефоны, музыка и видео, программное обеспечение, мебель, лекарства, парфюмерия, билеты, продукты питания, игрушки, одежда, платежные карты и другое. Покупатели в подавляющем большинстве проживают в крупных городах, таких как Москва или Киев. Платить предпочитают наличными курьеру, наложенным платежом, пластиковой картой, банковским переводом или электронными деньгами.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;По последним тенденциям видно, что из года в год увеличивается средняя стоимость покупки, а торговля в регионах развивается. Все это привело к очень бурному развитию торговли в Интернете. Уже сейчас есть много онлайновых торговых компаний, таких как &laquo;Amazon.com&raquo; (крупнейший в мире интернет-магазин &ndash; https://www.amazon.com/ с оборотом более 20 млрд. долларов в год), &laquo;OZON.ru&raquo; (крупнейший в России интернет-магазин &ndash; https://www.ozon.ru/ с оборотом более 100 млн. долларов), &laquo;ROZETKA&raquo; (крупнейший в Украине интернет-магазин &ndash; https://rozetka.com.ua/) и многие другие. В последнее время видна тенденция прихода в сеть крупных торговых сетей из off-line. Коммерческая часть или про особенности бизнеса Маркетинговые исследования Как и любой бизнес, создание интернет-магазина нужно начитать с маркетинговых исследований. Цель этого важного этапа &ndash; получить информацию, которая будет являться основой для успешного начала бизнеса. Нужно обязательно исследовать спрос на продаваемую продукцию, конкурентов, целевую аудиторию и другие важные аспекты. Правильный вариант &ndash; отдать исследования в маркетинговое агентство или, по крайней мере, проводить их совместными усилиями со специалистом по исследованиям. Эконом вариант &ndash; провести исследования собственными силами, которые тоже дадут много полезной информации.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Если уже есть четкое решение делать магазин, можно найти подрядчика, который сможет обеспечить и разработку, и продвижение магазина, после чего попросить помощи у него в проведении предпроектных исследований с целью создания качественного магазина. Совместными силами можно получить информацию о конкурентах и их преимуществах, узнать ожидаемый спрос, последние технические новинки в этой области и много другой информации. Вся эта информация должна учитываться как при разработки сайта магазина, так и при его продвижении. Коммуникация Следующий важный элемент данного бизнеса &ndash; организация удобной для потребителей коммуникации. Цель этапа &ndash; сделать много разных вариантов коммуникации, чтобы потребитель с любыми предпочтениями смог быстро связаться с менеджерами магазина. Первый канал коммуникации &ndash; это телефон, на него можно ожидать больше всего запросов. Сейчас с телефонами особых проблем нет: из любой точки мира можно принимать звонки и звонить по прямому московскому (495), киевскому (044) или другому номеру, и стоит это совсем недорого в зависимости от технологии (IP телефония, CDMA и др.) и телекоммуникационной компании &ndash; 5-30$ / мес. (например, интернет-магазин &laquo;Связной&raquo; &ndash; svyaznoy.ru принимает звонки круглосуточно). Можно организовать многоканальную горячую линию 0-800 (например, интернет-магазин &laquo;АЛЛО&raquo; &ndash; allo.ua имеет горячую линию). Важной частью в магазине является on-line чат, который позволяет общаться с помощью моментальных сообщений (например, сервис моментальных сообщений для сайта &laquo;Siteheart&raquo; - siteheart.com, использовать можно бесплатно). У клиентов часто возникают вопросы, и очень удобно сделав всего один клик, получить ответ на интересующий вопрос (например, интернет-магазин &laquo;SVEN&raquo; &ndash; shop.sven.ua имеет on-line чат). Следующее, на что стоит обратить внимание, &ndash; это месседжеры, типа ICQ, Skype и многие другие (например, интернет-магазин &laquo;Fotomag&raquo; &ndash; fotomag.com.ua предлагает связаться по ICQ). Зарегистрировать их не составляет труда. Однако для больших магазинов могу возникнуть трудности с распределением нагрузки на каждый контакт.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;В этом случае проблему можно решить технически: каждому посетителю показывается 1-2 контакта, но при этом они постоянно меняются (посетителю &laquo;А&raquo; показывают контакт &laquo;А&raquo;, посетителю &laquo;Б&raquo; показывают контакт &laquo;Б&raquo; и так по кругу), таким образом, нагрузка будет равномерно распределяться. Затем можно вспомнить старую технологию email, которая до сих пор хорошо себя чувствует. Минимум нужно создать один общий email и по одному для каждого менеджера по продажам, на сайте разместить форму обратной связи, которой до сих пор пользуются некоторые люди, а затем регулярно проверять почту. Покупатели часто пишут электронные письма и нередко не получают обратного ответа от магазинов &ndash; это большая ошибка, которая приводит к потере клиентов. Еще важным элементом коммуникации для интернет-магазинов являются социальные сети, но о них ниже. Менеджеры по продажам Немало людей почему-то думают, что для интернет-магазина почти не нужны люди, в частности, менеджеры по продажам. Это заблуждение. Любой магазин без профессиональных продавцов хорошо работать не сможет. Представим ситуацию, когда звонит сомневающийся потенциальный клиент и задает пару технических вопросов: &laquo;технарь&raquo; просто ответит на вопросы и попрощается, а &laquo;продажник&raquo; ответит на вопросы, затем выяснит потребности и подберет такой продукт, который максимально подойдет клиенту &ndash; конечно, разница огромная, особенно если перенести эту ситуацию на большой поток клиентов и конечные прибыли. Менеджеры по продажам в интернет-магазине должны быть и должны знать все особенности продаж через Интернет, потому что они значительно отличаются от традиционных продаж в реальном мире. Доставка По данным ROMIR покупатели выделяют 2 основных преимущества интернет-магазинов: 68% респондентов называют экономию времени и 54% экономию денег. Конечно, доставка в экономии времени играет большую роль, поэтому очень важно сделать её быстрой и удобной для пользователей. Покупатель должен зайти в магазин, быстро найти нужный товар, легко за него заплатить и быстро его получить в удобное ему время. Сейчас есть много служб доставки, которым это можно отдать на аутсорсинг (местные курьерские службы, междугородние, например, &laquo;Автолюкс&raquo; &ndash; https://www.autolux.ua/, &laquo;DHL&raquo; &ndash; https://www.dhl.com/, &laquo;Ночной экспресс&raquo; &ndash; https://www.nexpress.com.ua/ и другие). Для больших магазинов можно построить собственную службу доставки. На сайте это будет выглядеть как опция при заказе и отдельная страница с описанием доставки, а для магазина важно. Оплата Еще один важный для пользователей магазина элемент &ndash; оплата. В процессе оформления покупки у пользователя должна быть возможность выбора удобного варианта оплаты. Сам процесс может происходить по шагам: сначала выбираем сам способ оплаты (наличными, электронными деньгами, пластиковой картой и т.д.), затем система автоматически, в зависимости от выбранного способа, генерирует счет на оплату или предлагает выбрать электронную платежную систему или ввести данные карты и т.д. Суть в том, чтобы пользователь оплатил так, как ему удобно, и потратил минимум времени (например, интернет-магазин &laquo;Болеро&raquo; &ndash; http://www.bolero.ru/ предлагает много разных способов оплаты). Для организации таких платежей можно использовать популярные системы электронных платежей (например, &laquo;ASSIST&raquo; &ndash; https://www.assist.ru , &laquo;ChronoPay&raquo; &ndash; https://www.chronopay.com/ , &laquo;Интеркасса&raquo; &ndash;https://www.interkassa.com/ и другие; все они берут процент за услуги посредничества) или сделать все без посредников, прямые приемы платежей. Сезонность Стандартная особенность торгового бизнеса - в разное время года будет разный уровень продаж. Как правило, традиционные бизнес сезоны весна и осень, в это время продажи выше, а &laquo;несезон&raquo; лето и зима. Также продажи некоторых категорий товаров растут в праздники (например, интернет-магазин &laquo;GOLD.ua&raquo; &ndash; gold.ua предлагает подарки к традиционным праздникам на 23 февраля и 8 марта и даже выделяет это в контекстном меню слева). Сезонность нужно учитывать не только в планировании продаж, но и перед запуском магазина.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Разработку нового магазина стоит начинать не позже, чем за 3 месяца до начала сезона, иначе можно не успеть к началу сезона закончить достижение высокого уровня продаж и потерять прибыль. Сайт магазина Один из важнейших элементов успешного магазина. &laquo;Сайт должен эффективно превращать посетителей в покупателей&raquo; &ndash; стандартная фраза многих разработчиков, за которой скрывается важный смысл: на одних сайтах потенциальный покупатель быстро и без колебаний совершает покупку, а на других не может найти нужный товар, хотя он там есть. Для измерения эффективности сайта обычно используют понятие &laquo;коэффициент конверсии&raquo;, который показывает соотношение посетителей к покупателям. На некоторых сайтах этот коэффициент достигает значения 20% и более, т.е. из 100 посетителей &ndash; 20 совершают покупку. Конверсия будет высокой, если сайт магазина сделан качественно, на него заходят потенциальные клиенты, а лояльность клиентов высокая, обо всем этом ниже. Существует интересная маркетинговая модель AIDA (Attention, Interest, Desire, Action &ndash; внимание, интерес, желание, действие), которая активно используется на практике в США. Её суть состоит в том, чтобы заставить посетителя пройти 4 шага: сначала обратить его внимание на продукт, затем заинтересовать его продуктом, после вызвать желание получить продукт и в конце заставить сделать действие (в нашем случае купить продукт). Весь сайт должен строиться по этой модели, она очень помогает увеличивать число покупателей, но, к сожалению, очень немногие разработчики знают о существовании этой модели. Внешне магазин должен обязательно вызывать доверие, этому способствует много факторов: известность, возраст бизнеса, хороший дизайн, сертификаты от производителей и т.д. Для Интернета это очень важные элемент, сейчас у многих пользователей рунете все еще развито большое недоверие к on-line покупкам. Конверсия Этому показателю стоит уделить особое внимание. О нем придется думать всегда: при разработке сайта, в процессе продвижения и в будущих прогнозах. Конверсия &ndash; универсальное мерило эффективности интернет-магазина. Средний уровень конверсии в рунете 1-2%, т.е. из 100 посетителей покупку сделают всего 1-2 человека. На западе этот показатель значительно выше. Связано это, прежде всего, с неправильным подходом к построению этого бизнеса у нас, вернее к банальному неумению, но об ошибках расскажу ниже. В тоже время в некоторых магазинах конверсия превышает 20% и более (например, по данным издания &laquo;MarketingCharts&raquo; &ndash; https://www.marketingcharts.com/ , некоторые магазины достигают конверсию в 20, 30 и даже 40%; один из таких магазин &laquo;ProFlowers&raquo; &ndash; proflowers.com в ноябре 2009 получил конверсию в 20,1%, а в декабре &ndash; 22%, и на таком уровне конверсии он находится минимум 2 года! И, к слову сказать, магазин очень популярный&hellip;). Очень важным для конверсии будет цена: чем она выше, тем меньше конверсия. Это объясняет высокую конверсию на сайтах с недорогой продукцией (например, цветы) и низкую с дорогими товарами (например, автомобили). Конверсия зависит, прежде всего, от 3-х глобальных частей: Бизнес. Бренд, цены на продукты, качество продуктов, условия оплаты, доставки, гарантия и т.д. Сайт. Насколько сайт подходит для продаж, что продает, какой у него функционал, дизайн и т.д. Продвижение.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Кто приходит на сайт, какая лояльность у посетителей, что ищут и т.д. Если упустить из виду хотя бы одну из 3-х частей &ndash; конверсия будет низкой. Но, как ни странно, у нас умудряются упустить и 1-ю, и 2-ю, и, в особо безнадежных случаях, даже 3-ю части волшебной формулы. Другое Конечно, выше описаны не все особенности, и, конечно, есть еще стандартные моменты торгового бизнеса: реальный офис, логистика, кредитование покупателей, программы лояльности и много-много других важных моментов. Главное, чтобы бизнес был целиком конкурентоспособным на рынке, только в этом случае можно работать над сайтом и его продвижением, и именно поэтому сильно выигрывают большие розничные сети магазинов из реального мира, которые много лет работали над увеличением конкурентоспособности и собственным брендом; таким гигантам достаточно только грамотно интегрироваться в сеть. Техническая часть или про создание сайта Хостинг и домен Для обычного интернет-магазина с большим количеством товара и высокой предполагаемой посещаемостью подойдет простой хостинг на 5000 мб с неограниченным трафиком (100-150$ / год), для большого гипермаркета нужно брать VDS (от 200$ / год) или выделенный сервер (от 1500$ / год): он и для нагрузки нужен, и много место на диске требуется. Доменное имя должно быть простое, хорошо запоминающееся и желательно отражать смысл магазина (например, книжный интернет-магазин &laquo;Books.Ru&raquo; &ndash; https://www.books.ru/ , сразу понятно, что там продают, и складываются правильные ассоциации). Платформа Сейчас есть очень много разных решений для интернет-магазинов. У всех есть преимущества, недостатки и особенности. Первый вариант &ndash; создать собственную систему управления сайтом (CMS) под свои требования. Этот вариант подходит в основном для очень больших или нестандартных интернет-магазинов, в которых нужно учитывать много нестандартных функций, что лучше всего делать с нуля. Однако у этого варианта много недостатков, стоит выделить хотя бы 3 основных: Стоимость. Конечно, создавать новое качественное решение с нуля всегда намного дороже, это, наверное, основной недостаток. Сроки. В этом случае они увеличатся в несколько раз по сравнению с разработкой на коробочном решении. Безопасность.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Если многие коробочные CMS уже прошли кучу тестов на безопасность, то в собственной системе этот момент нужно будет учитывать своими силами. Из-за этого вариант разработки собственной платформы используется очень редко, тем более, что есть много качественных коробочных решений с открытым кодом, которые можно самостоятельно дорабатывать. Второй вариант &ndash; использовать бесплатное коробочное решение. Сейчас есть много бесплатных CMS с открытым исходным кодом, в том числе и специализированных для интернет-магазинов. Одним из основных преимуществ такого решения является цена &ndash; стоимость разработки будет низкая, а за саму лицензию платить ничего не нужно. Недостатков впрочем, намного больше: отсутствие технической поддержки, часто функциональность хуже платных аналогов, могут быть серьезные проблемы с безопасностью, что для магазинов особенно плохо. Разных систем много, сразу можно посмотреть в сторону: &laquo;osCommerce&raquo;, &laquo;PHPShop&raquo;, &laquo;Joomla!&raquo;, &laquo;Drupal&raquo; и др. Третий вариант &ndash; платное коробочное решение. Выбор таких решений на рынке очень большой, с разной функциональностью и разными ценами. Преимуществ много: официальная техническая поддержка, хорошая функциональность, высокая безопасность и производительность, бесплатные обновления и т.д., основной недостаток один &ndash; за лицензию нужно заплатить, а конечная стоимость разработки будет больше, чем при разработке на бесплатном аналоге. В этой категории стоит обратить внимание на: &laquo;1С-Битрикс&raquo;, &laquo;Amiro&raquo;, &laquo;HostCMS&raquo;, &laquo;NetCat&raquo; и др. Любой из 3-х вариантов имеет своих потребителей: свою платформу делают большие проекты, бесплатную CMS выбирают малобюджетные проекты, а платную - средние по размеру магазины с хорошими требованиями к качеству. Я рекомендую смотреть в сторону третьего варианта.</p>', 'posts/April2021/QHKvC9XPV6FG912Yim8b.jpg', 'sozdanie-internet-magazina', 'разработка сайта, разработка лендинг пейдж, создание сайтов, разработка корпоративных сайтов', 'разработка сайта, разработка лендинг пейдж, создание сайтов, разработка корпоративных сайтов', 'PUBLISHED', 0, '2021-03-30 05:37:10', '2021-04-28 23:07:17', 0, 1),
(2, 1, 1, 'Лендинг или многостраничный сайт', '', 'На самом деле, чем бы вы ни занимались, для того, чтобы привлечь как можно больше потенциальных клиентов в ваш бизнес, вам необходим свой собственный сайт...', '<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Вы продаете автомобильные покрышки? Супер. Торгуете детскими игрушками? Замечательно. А может, у вас цветочный бизнес? На самом деле, чем бы вы ни занимались, для того, чтобы привлечь как можно больше потенциальных клиентов в ваш бизнес, вам необходим свой собственный сайт. На сегодняшний день более 70% всех решений о покупке принимаются в той или иной степени через интернет, поэтому целесообразность создания сайта становится понятной: люди могут просто пройти мимо вашей компании, обратившись в ту, которая разместилась в интернете. Итак, в необходимости создания сайта мы убедились. Однако, их существует несколько типов: многостраничный сайт; посадочная страница (он же лендинг); интернет-магазин; Об интернет-магазине мы подробнее расскажем в следующих статьях, пока остановимся на оставшихся двух видах сайтов. Так как понять, какой вариант сайта подходит именно для вашего бизнеса? Прежде всего, нужно понимать , что каждый вид сайта преследует разные цели, поэтому, прежде чем определиться с выбором, необходимо их чётко обозначить. Лендинг или многостраничник? Рассмотрим основное назначение, отличия и цели лендинга и многостраничника. Лендинг (LandingPage, Лендос, LP) &ndash; упрощенная версия сайта, используемая, как правило, для выполнения одной функции: сбора данных потенциальных клиентов. Иначе говоря, это онлайн-презентация вашего товара/услуги, которая кратко расскажет о преимуществах и предложит оставить заявку (имя, телефон, емайл и тд). Последняя либо приходит к вам на почту, либо падает в вашу CRM-систему, где автоматически становится лидом(заявкой), и вы или ваш отдел продаж начинает с ней работать.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Естественно, что лендинг должен максимально выгодно доносить до посетителя то предложение, которое в нем обозначено. Успех лендинга напрямую зависит от того, насколько в компании понимают желания клиента и, конечно, насколько &laquo;вкусно&raquo; эти желания преподносят. Донесите до вашего клиента, что именно с вашей помощью он приобретет действительно качественную вещь/услугу&hellip; Клиент должен быть уверен в надежности вашего предложения, тогда он обязательно сделает выбор в вашу пользу и нажмет кнопку &laquo;Оставить заявку&raquo;. Преимущества и недостатки лендинга Лендинги, как и любые сайты, обладают плюсами и минусами. Среди существенных плюсов &ndash; это, конечно, невысокая цена, либо вообще ее отсутствие. Сегодня существует достаточно конструкторов для простого и быстрого создания лендингов самостоятельно. Например, Сбербанк, Битрикс24 и др. предлагают бесплатные платформы. Тем не менее, лендинги, сделанные специалистами, генерируют больше заявок и, соответственно, приносят больше прибыли, чем их бесплатные собратья. Также большим плюсом по сравнению с многостраничниками, для лендингов будет большая конверсия. Ведь посетителю не предоставляется выбора, что делать на сайте. Он двигается по заранее намеченному вами пути, либо, если его не заинтересовало предложение, сразу выходит и вряд ли уже вернется. Сегодня хорошим уровнем конверсии лендинга можно считать 5-10% (из 100 привлеченных на ваш лендинг посетителей заявку оставят от 5 до 10 человек). В плюсы запишем и то, что лендинг легко протестировать. Как это сделать? Либо прочередуем трафик на тестируемые версии (например, 2 недели на один вариант, потом 2 недели на второй вариант и сравнить показатели), либо разделим весь входящий трафик на оба тестируемых варианта и так же сверяем результаты. Какой вариант показал себя лучше тот и выбираем. Делая такое тестирование постоянно, можно увеличить показатели конверсии в 2-3 раза за пару месяцев. Среди минусов - платные каналы привлечения клиентов . Чтобы получать клиентов с лендинга, необходимо постоянно тратить деньги на рекламу, поскольку лендинг очень плохо привлекает бесплатный (органический) трафик. Поэтому, как правило, если у вас закончились деньги на рекламу &ndash; то и закончились заявки с лендинга. Для эффективной отдачи от лендинга необходимо использовать такие платные источники рекламы, как Яндекс.Директ, Google Adwords, таргетированную рекламу. Минусом для компании, которая предлагает широкий выбор услуг, может также оказаться и то, что лендинг представляет какой-то единичный товар или услугу. Поэтому, если у вас юридическая компания и вы занимаетесь возвратом страховки с банков, трудовыми спорами и взыскиваете недоплату по ОСАГО, то вам понадобятся лендинги под каждую вашу услугу. Ведь у всех у них своя целевая аудитория, стимулировать которую к контакту с вами необходимо по-разному.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Если же ваша компания занимается установкой натяжных потолков ,то лендинг подойдет вам идеально, однако при его разработке нужно будет максимально выложиться &ndash; ничего не поделаешь, в этой сфере большая конкуренция, и чем грамотнее будет сделан ваш лендинг, тем больше клиентов он привлечет. Еще один минус &ndash; небольшой срок жизни лендинга ( в среднем, 1-1,5 года).Ведь технологии, рынок, да и ваши конкуренты не стоят на месте. Поэтому, для поддержки высоких показателей конверсии лендинга, необходимо его пересобирать хотя бы 1 раз в год, учитывая актуальную на данный момент конъюнктуру рынка. Минусом лендинга также является его шаблонность . С ним действительно сложно выделиться &ndash; ведь многие компании, занимающиеся созданием лендингов, имеют схожую &laquo;канву&raquo;: главный экран, заголовок, УТП, формы захвата, отзывы, благодарности, примеры работ, цифры и факты о компании и тд. В итоге часто получаются схожие продукты, которые различаются лишь цветовым оформлением, заголовком и фоном. Поэтому посетителям становится трудно выделить тот или иной лендинг в общей массе. Чтобы выделиться среди конкурентов необходимо вложить в лендинг что-то особенное. Буквально частичку своей души. Минусом ( но также его можно рассмотреть и как плюс) будет и ограничение по объему контента. До последнего экрана лендинга доходит менее 2% от общего числа посетителей, поэтому требуется уложить восприятие вашего контента всего в 30-60 секунд. Если же вам требуется больше времени, чтобы рассказать о своем предложении &ndash; задумайтесь &ndash; может, вам нужен полноценный многостраничный сайт. Пожалуй, существенным минусом лендингов будет то, что в силу маленького объема контента, одностраничности, низкого срока жизни, их не слишком жалуют поисковые системы. Поэтому, имея лендинг, практически невозможно оказаться вверху поискового списка.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Однако, если разместить информацию о вашем сайте на бесплатных площадках региона, таких как 2гис, Яндекс.Справочник и др., это поможет привести на ваш лендинг несколько больше посетителей. О плюсах и минусах многостраничников Плюс многостраничника в том, что ему всегда есть куда расти, развиваться , &laquo;расцветать&raquo;. В них постепенно можно привносить что-то новое, усовершенствовать, делать удобнее для клиентов. В сознании клиентов многостраничник видится &laquo;весомее&raquo;, убедительнее, чем лендинг, что, несомненно, является его преимуществом перед LP. Так как многостраничник приводит больше клиентов, то это снижает &laquo;стоимость&raquo; каждого из них в отдельности. Конечно, это тоже плюс. Еще в плюсы запишем то, что для успешного продвижения сайта подойдут почти все каналы продвижения, а самый лучший и действенный метод - это комбинирование каналов привлечения клиентов. Отлично подойдут даже социальные сети - это естественная среда обитания клиента, где ему комфортно принимать решения на своей территории и в удобное для него время. Действенным также может оказаться и е-мейл маркетинг. К минусам сайтов можно отнести долгий старт, довольно дорогое продвижение и более низкую конверсию по сравнению с лендингом (которая, однако, компенсируется за счет органического трафика).</p>', 'posts/April2021/bPOAL0MlK3jICAWPGIAj.jpg', 'lending-ili-mnogostranichnyj-sajt', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-03-30 05:37:10', '2021-04-28 23:08:10', 1, 1),
(3, 1, 1, 'SEO-тренды 2020 и советы по их применению', '', 'Yandex и Google активно внедряют новые технологии в поиск. А SEO-специалисты пытаются подготовиться и адаптировать свою работу к этим изменениям. Так и возникают новые SEO-тренды, применение которых может дать хороший эффект в виде роста трафика из поиска на ваш сайт. В этой статье речь пойдет о нескольких, самых значимых, по нашему мнению, seo-трендах 2020 года.', '<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Yandex и Google активно внедряют новые технологии в поиск. А SEO-специалисты пытаются подготовиться и адаптировать свою работу к этим изменениям. Так и возникают новые SEO-тренды, применение которых может дать хороший эффект в виде роста трафика из поиска на ваш сайт. В этой статье речь пойдет о нескольких, самых значимых, по нашему мнению, seo-трендах 2020 года. 1. Скорость загрузки Техническое SEO-продвижение было и остается одним из главных элементов продвижения любого проекта. Главная задача технической оптимизации &mdash; сделать ваш сайт доступнее для индексации поисковыми роботами. Также важно, чтобы сайт быстро и без проблем загружался и отображался у пользователей. В 2020 году советуем обратить особенное внимание на скорость загрузки страниц. Google и Yandex учитывают скорость загрузки при ранжировании, пессимизируют очень медленные ресурсы. В 2020 году выдача будет состоять из быстрых сайтов. Специалисты надеются, что большинство оптимизаторов займется ускорением загрузки. 2. Доверие и репутация бренда Доверие пользователя к сайту напрямую зависит от репутации компании и авторитетности в определенной сфере. В первую очередь нужно работать с отзывами о компании. Поисковые системы в последнее время всё чаще двигают в ТОП сайты с положительными отзывами. Размещены они могут быть как на внешних площадках (каталоги, справочники), так и в сервисах Яндекс.Справочник и Google My Business.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;С последними советуем работать очень плотно, поскольку они показывают отзывы о вашем сайте сразу на странице выдачи. 3. Экспертность Кроме предоставления интересного и вовлекающего контента нужно подтверждать его экспертность. Особенно это рекомендует Google. При создании контента старайтесь задействовать экспертов в вашей области. Если это автор, который пишет статьи на сайте, то сделайте для него отдельную страницу, на которой будет указана информация о его опыте, участии в конференциях, семинарах и представлены сертификаты, грамоты, дипломы. У Google даже есть свои способы определения авторства. В качестве примера в руководстве для асессоров Google приводит рекомендацию в медицинской тематике. 4. Понимание и учет интента поисковых запросов В среде seo-специалистов есть отдельный термин &mdash; интент.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Это намерения пользователя, то, что он держит у себя в голове, когда вводит поисковый запрос. Интент по сути &mdash; это и есть потребность, которую пользователь хочет решить при вводе запроса в строку поиска. Большее внимание поисковиков к интентам означает, что высоко будут ранжироваться сайты, которые максимально ориентированы на эти потребности пользователей и готовы реально решать задачи аудитории. По прогнозам в 2020 году этот фактор станет ключевым и будет играть намного большую роль, чем вхождения ключевых слов в текст страницы, в описание или даже в заголовок. Чтобы привести свой проект в соответствие этому тренду, надо научиться понимать: что хочет получить в ответ на запрос ваша аудитория; какой контент для нее важен; в каком виде пользователи ожидают его увидеть в ответе. И, конечно, постараться на своем сайте сформировать этот ответ в максимально подробном виде.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Разберем это на конкретном примере: Допустим, пользователь набирает запрос &laquo;мебель для детской комнаты&raquo;. Что он хочет увидеть на сайте, который появится в ответе? Множество изображений мебели и ее элементы. Чем больше этих вариантов он увидит, тем лучше, так как пользователю важна возможность выбора. Габаритный размер. Чтобы понять, какой вариант подойдет, нужно понимать размер этой мебели. Поэтому очевидно, что рядом с изображением было бы правильно указать размеры элементов. Цену. Стоимость товара или услуги всегда была и остается важным параметром выбора пользователя. Отсутствие цены часто приводит к закрытию страниц сайта в первые секунды. В нашем блоге есть отдельная статья, в которой мы подробно описали важность этого параметра на посадочных страницах. Материал.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Напомним, что мы ищем именно мебель для детской комнаты. Пользователям может быть важно, чтобы материал, из которого эта мебель изготовлена, соответствовал экологическим требованиям и стандартам. Функции сортировки и подбора по параметрам. Чтобы пользователю было удобно сортировать ассортимент по нужным параметрам, на странице должны быть функции подбора. Хотя бы по цене. Если есть возможность подбора по цвету или габаритам &mdash; это только плюс, так как выбирать становится еще удобней. Возможность заказа. Это может быть просьба о звонке менеджеру, корзина, запрос обратного звонка, заказ в 1 клик и любые другие призывы к действию. Главное, чтобы они были на видном месте и работали быстро и без ошибок.</p>', 'posts/April2021/8KSGFGUK2hECBkLKRjHh.jpg', 'seo-trendy-2020-i-sovety-po-ih-primeneniyu', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-03-30 05:37:10', '2021-04-28 23:08:45', 0, 1),
(4, 1, 1, 'КАК ФОРМИРУЕТСЯ СТОИМОСТЬ САЙТА И ПОЧЕМУ ЛУЧШЕ ЗАКАЗЫВАТЬ ЕГО РАЗРАБОТКУ ЧЕРЕЗ АГЕНТСТВО', '', 'Вам необходим сайт, вы задумываетесь о его разработке, но не знаете, как лучше это сделать и где заказать, давайте разбираться.\n', '<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Вам необходим сайт, вы задумываетесь о его разработке, но не знаете, как лучше это сделать и где заказать, давайте разбираться. За что платим при создании сайта? Стандартная схема разработки сайта в любой крупной интернет-компании включает в себя несколько основных этапов: Предварительная подготовка Предпроектный анализ. Ознакомление с заказом, постановка целей и задач, изучение специфики работы клиента, анализ рынка и конкурентов. Поиск и исследование целевой аудитории. Составление графика работ и расчёт затрат. Разработка ТЗ, состоящего из требований к технической части сайта и его дизайну. Создание карты сайта. Разработка и утверждение дизайна Визуальная составляющая сайта. Разработка уникальной идеи, основного графического оформления сайта.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Разработка фирменного стиля и его отдельных элементов. Технический дизайн. Создание графических шаблонов для страниц сайта. Верстка Верстка html-страниц по выбранному дизайну для типовых страниц. Программная работа Объединение сайта с CMS. Настройка сервера, программирование, создание условий для безопасной работы проекта. Запуск сайта, доработка функционала. Наполнение сайта информацией Размещение на сайте тематического контента, текстов и изображений. Тестирование сайта в интернете Проверка работы сайта, наличия ошибок и сбоев. Тестирование html-страниц на правильность работы в разных браузерах. Сдача сайта в работу Проведение работ по размещению сайта в сети на домене клиента. Итоговое тестирование проекта. Консультирование клиента.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Оптимизация под поисковые системы Комплекс работ по продвижению сайта в популярных поисковых системах. Что выбрать &laquo;Интернет-агентство&raquo; или &laquo;Фриланс&raquo;? При создании сайта практически во всех случаях возникает вопрос, где лучше заказать эту услугу. Обратиться можно как в агентство, так и к фрилансерам, занимающимся созданием веб-проектов самостоятельно. Если вам необходим качественный ресурс, а не простой одностраничный сайт или блог, пытаться сделать его самостоятельно с помощью бесплатных движков не стоит, т.к. у них есть ряд ограничений по функционалу и продвижению. Только профессионалы могут грамотно разработать и оптимизировать многостраничный сайт или современный интернет-магазин на движке (CMS) отвечающим всем современным требованиям создания и продвижения сайта. Выбирая между услугами веб-студий и фрилансеров, нужно отталкиваться от бюджета на создание сайта, сроков реализации проекта и его уровня сложности. Услуги фрилансера не всегда, но часто могут стоить дешевле (до 1500 рублей за 1 час работ). Однако ему может понадобиться больше времени на создание сайта, так как работать над ним он будет в одиночку. Не исключен и человеческий фактор с задержкой создания проекта и продления сроков. У фрилансера могут возникнуть какие-то сложности в процессе работы.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;В результате подобных обстоятельств он будет просить увеличить сроки работ. В худшем случае фрилансер может просто отказаться от выполнения работ, и заказчику придётся срочно искать другого исполнителя или обращаться в специализированные агентства. Также, не редки случаи, когда после сдачи &laquo;сырого&raquo; сайта исполнитель-фрилансер просто пропадал и не выходил на связь. В интернет-агентстве совершенно другие условия работы с заказчиком. В процессе создания сайта принимают участие сразу несколько сотрудников, которые ведут проект от начала и до завершения всех работ по его созданию. Стоимость работ может быть немного выше, но и качество их выполнения будет на другом уровне. Также, между заказчиком и исполнителем заключается договор, который гарантирует выполнение поставленных задач и соблюдение сроков. Единственным недостатком заказа разработки сайта в интернет-агентстве может стать более высокая стоимость услуг в зависимости от сложности (примерно 1500-2500 рублей за 1 час), но и она покрывается гарантиями, что работа будет выполненна качественно и в срок и никто никуда не пропадет. В остальном от сотрудничества с профессиональными IT-компаниями заказчики получают только плюсы. Разработка сайтов &laquo;под ключ&raquo; также является важным преимуществом агентств. Они используют комплексный подход к созданию веб-проектов, разрабатывая их структуру совместно с заказчиком и сопровождая сайты даже после завершения всех необходимых действий для их создания и оптимизации.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Зачастую веб-студии являются официальными представителями какой-то определённой системы управления сайтами (1С-Битрикс, NetCat и др.), в этом случае заказчик может рассчитывать на полный профессионализм команды и возможную скидку за выбор этой CMS для своего сайта. Вместо заключения Сайты, относящиеся к сегменту эконом, можно собрать, как при помощи агентства, так и через сотрудничество с фрилансером. При этом можно сэкономить. Однако стоит учитывать, что для создания любого современного адаптивного сайта, который будет удобно использовать на любом типе устройстве, будь то ПК, ноутбук, планшет или смартфон, необходим комплекс работ, и искать придётся сразу нескольких фрилансеров. Их работу будет необходимо координировать. Что не всегда представляется возможным. Делая заказ на разработку сайта в веб-студии, вы получаете полный комплекс работ от специалистов, гарантию соблюдения всех сроков и фиксированную, оговоренную заранее стоимость работ. Главным преимуществом в этом случае является гарантия качества и дальнейшего сопровождения проекта. В итоге стоит сказать, что к фрилансерам лучше обращаться в тех случаях, когда необходим более простой сайт. В то время как в агентстве можно смело заказывать сайт любого уровня сложности и при этом иметь больше гарантий, что ваш проект станет уникальным и запустится в работу без задержек и перенесения сроков.</p>', 'posts/April2021/AdG8apCsTvGmZSyFNX4D.jpg', 'kak-formiruetsya-stoimost-sajta-i-pochemu-luchshe-zakazyvat-ego-razrabotku-cherez-agentstvo', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-03-30 05:37:10', '2021-04-28 23:09:13', 0, 0),
(5, 1, 1, 'Оптимизация картинок на сайте', '', 'Картинки привлекают дополнительный трафик. Они влияют на скорость загрузки страниц и поведенческие факторы. При качественной оптимизации они могут улучшить рейтинг вашего сайта в поисковиках.', '<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Почему так важно оптимизировать картинки? Человек часто не читает и не останавливает свое внимание на тексте, обычно он &laquo;сканирует&raquo; страницы. Зачастую обращает внимание на графические элементы. На основе такого поведения поисковые системы получили ряд критериев, по которым анализируют изображения. Они сводятся к улучшению качества контента в результатах поиска и удовлетворению запросов пользователя. Поэтому кроме наличия изображений на сайте, важно, чтобы картинки быстро загружались, были привлекательными, информативными и качественными. После появления в SERP Google расширенного сниппета, стало очень просто попасть на первую страницу с помощью картинок. Причем часто встречаются ситуации, когда картинка показывается вместе с контентом конкурирующего сайта. Появляется шанс увеличить видимость и узнаваемость сайта с помощью картинок. Чтобы попасть в такие результаты, нужно провести комплексную оптимизацию изображений. Важно помочь поисковым системам правильно анализировать содержимое картинки. Каковы основные требования к созданию картинок? В целом основные требования к созданию изображений сводятся к таким параметрам: формат, качество и размер картинок. #1 Формат изображений Google может индексировать типы изображений в форматах BMP, GIF, JPEG, PNG и WebP, а также SVG. JPEG &mdash; используйте этот формат для фото; PNG &mdash; для графики, проще говоря, для всего, что нарисовал дизайнер; SVG &mdash; для векторных изображений. Появились и новые форматы, такие, как WebP и JPEG-XR. Их преимущество в том, что они действительно меньше весят, но, к сожалению, пока не все браузеры поддерживают эти форматы. Например, JPEG-XR поддерживает только IE, а WebP &mdash; Chrome, Opera, Android.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; В связи с этим я не рассматриваю их. Но выбор за вами :) Для индексирования картинок в Яндексе стоит выбрать такие форматы: JPEG, GIF и PNG. Кроме правильного выбора формата, стоит учесть, что изображения, которые загружаются на странице при помощи скрипта, Яндексом не индексируются. #2 Качество При оптимизации картинок для Яндекс и Google, следует найти оптимальное решение между размером и качеством графики, поскольку, прежде всего контент предназначен для пользователя. Следите за тем, чтобы графика была четкой и визуально привлекательной. Некачественным изображением также считается картинка, не соответствующая своему описанию или расположенная около несвязанного по смыслу текста. Это изображение попало в результаты поиска по запросу &laquo;белые собаки&raquo;, что не соответствует реальному содержимому из-за неправильного описания и содержания текста статьи. Более подробно о том, как правильно писать описание к изображению будет описано дальше в этой статье. #3 Размер и вес изображений Размеры картинок влияют на скорость загрузки страницы, а та, в свою очередь, на ранжирование страницы. Если вы используете много изображений на странице, это может значительно замедлить ее загрузку. Существует множество инструментов и способов, как сжать фотографии без потери качества (о них я расскажу чуть позже). Используя их, будьте осторожны &mdash; сверяйте оригинальную картинку и сжатую. Изображения нужно создавать в размере, в котором они будут представлены на сайте. Браузеру будет легче сканировать контент страниц, если в CSS прописать ширину и высоту изображения. Для дисплеев с ретиной добавляйте изображения в размере 2x и настройте отображение разных размеров одного и того же изображения для разных экранов. Иначе для пользователей, которые откроют изображение на дисплее с ретиной, все картинки будут отображены с визуальной потерей качества.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Проверить размер и вес картинок можно с помощью инструмента &laquo;Аудит сайта&raquo; Serpstat. Сервис покажет список ошибок, которые сгруппированы по приоритету важности. Чтобы посмотреть все страницы, которые лучше сжать, перейдите в блок &laquo;Мультимедиа&raquo;, в нем собраны страницы с битыми и слишком большими изображениями. Для быстрого анализа изображений на одной странице можно воспользоваться расширением Serpstat Website SEO Checker. Размещение картинок Для корректной привязки изображений к вашему сайту храните их на своем домене или одном из поддоменов. Иначе пользователи, использующие поиск по изображениям, не смогут попасть на ваш сайт: они будут видеть адрес того ресурса, на котором хранится найденная картинка, даже если она находится на странице вашего сайта. Чтобы найти неполадки со скоростью загрузки и оптимизацией изображений для мобильных, используйте модуль &laquo;Аудит сайта&raquo; Serpstat. Помимо самих проблем вы найдете подсказки для их исправления.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Как уменьшить вес фотографии без потери качества? Делайте интернет быстрее, и гугл вам скажет &laquo;Спасибо!&raquo; Ловите несколько облачных сервисов на которых можно уменьшить размер фото онлайн. 1 Compressor &mdash; это бесплатный сервис, но есть лимит по размерам загруженного файла в 10MB. Возможны варианты сжатия с потерей и без потерь качества. В последнем случае функция доступна только для JPG и PNG форматов. С помощью этого сервиса возможно сжатие до 90%. 2 С помощью веб-интерфейса Kraken возможно оптимизировать как один, так и несколько файлов. Доступна функция оптимизации изображений веб-ресурса: достаточно ввести url сайта и на выходе получить архив со сжатыми изображениями. Также сервис предлагает ряд полезного функционала по оптимизации графики, такие как изменения размера изображений, API для веб-ресурсов и плагины для платформ WordPress и Magento. Аналогичные сервисы TinyPNG, JPEGMini позволяют сжать размер фото, но в основном используют сжатие с потерями, что снижает качество изображения. 3 Есть решение в виде программного обеспечения ImageOptim для Mac, который по умолчанию работает без потерь и снижения качества. ImageOptim может уменьшить размер фотографии онлайн для форматов JPEG, SVG, GIF и PNG. Простой и удобный интерфейс позволяет сжимать сразу несколько файлов.</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Принцип работы ImageOptim &mdash; удаление метаданных, таких как местоположение GPS и серийный номер камеры. Так что вы можете публиковать изображения без предоставления личной информации, которая добавляет веса графике. Сжатие на сервере Gzip: Настройте веб-сервер для сжатия JPEG-файлов в формат Gzip. PageSpeed: Для Nginx и Apache доступен модуль PageSpeed для сжатия данных на уровне сервера. Рекомендации по установке здесь. Для отслеживания и анализа размера изображений рекомендую использовать онлайн-инструменты Pingdom и PageSpeed Insights. Имеет смысл использовать оба варианта, но если вам нужно сосредоточиться на одном, все же лучше полагаться на Google Pagespeed Insights. Поскольку Google &mdash; это тот, кто сканирует контент и оценивает рейтинг вашего сайта. Рассмотрим пример использования PageSpeed Insights. Анализируя страницу, инструмент дает перечень рекомендаций о ресурсах, которые стоит уменьшить и насколько. Кроме анализа PageSpeed Insights предоставляет инструмент по сжатию ресурсов на основе своих рекомендаций, в том числе и изображений. Но на практике, уменьшение размера фото онлайн происходит с изменением размера и ухудшением качества.</p>', 'posts/April2021/H8FT3YTOTn3BRvMz6Y1d.png', 'optimizaciya-kartinok-na-sajte', '', '', 'PUBLISHED', 0, '2021-03-30 23:48:04', '2021-04-28 23:05:42', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_type_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `projects`
--

INSERT INTO `projects` (`id`, `project_type_id`, `title`, `slug`, `image`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 4, 'Wellness', 'landingpage-wellness-igra-na-snizhenie-vesa', 'projects/April2021/EFjp0DXTXTKwj7pSidyb.jpg', 'Разработка одностраничного сайта тематики \"Wellness\"', NULL, '2021-04-04 10:24:26', '2021-04-28 03:29:13'),
(2, 1, 'Апистар', 'maksimalnaya-zashchita', 'projects/April2021/XxdslxBNh91bnY9OdKTe.jpg', 'Разработка сайта компании \"Апистар\"', NULL, '2021-04-04 14:11:34', '2021-04-28 03:29:48'),
(3, 1, 'Пиццерия', 'la-jole', 'projects/April2021/8X6Q05UijVgfF2qIjWOk.jpg', 'La Jole', NULL, '2021-04-04 14:12:13', '2021-04-28 01:20:59'),
(4, 5, 'Frezerovka', 'your-puzzles', 'projects/April2021/5FgW9BgxYUP1cPqVRJoK.jpg', 'Frezerovka', NULL, '2021-04-06 04:52:38', '2021-04-28 01:20:42');

-- --------------------------------------------------------

--
-- Структура таблицы `project_types`
--

CREATE TABLE `project_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `project_types`
--

INSERT INTO `project_types` (`id`, `title`, `slug`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Логотипы', 'logotipy', NULL, '2021-03-31 02:23:10', '2021-03-31 02:23:10'),
(2, 'Landing Page', 'landingpage', '2021-03-31 02:24:52', '2021-03-31 02:23:54', '2021-03-31 02:24:52'),
(3, 'LandingPage', 'landingpage', '2021-03-31 02:25:34', '2021-03-31 02:25:06', '2021-03-31 02:25:34'),
(4, 'LandingPage', 'landing-page', NULL, '2021-03-31 02:25:22', '2021-03-31 02:25:42'),
(5, 'Мобильные приложения', 'mobilnye-prilozheniya', NULL, '2021-03-31 02:26:08', '2021-03-31 02:26:08');

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `stars` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `reviews`
--

INSERT INTO `reviews` (`id`, `stars`, `title`, `slug`, `review`, `created_at`, `updated_at`, `image`) VALUES
(1, 4, '\"КазБиоТранс\"', 'ooo-tulskiy-zavod', 'Заказали разработку сайта, довольно сложного по функционалу. Компания предоставила просто отличный сервис и от качество сайта на высшем уровне', '2021-04-04 15:20:00', '2021-04-22 23:54:03', 'reviews/April2021/gyEZhM4J7xFfI7EL25qp.jpeg');

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2021-03-30 05:37:09', '2021-03-30 05:37:09'),
(2, 'user', 'Normal User', '2021-03-30 05:37:09', '2021-03-30 05:37:09');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', 'G-S8PMJ2BL3W', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin'),
(11, 'glavnaya-stranica.info', 'О нашей компании', 'Мы - команда профессионалов, работающая на результат и качественно исполняющая все обязанности перед Вами. \r\nХотите поднять свой бизнес на новый уровень? \r\nВаши поиски завершены.', NULL, 'text_area', 6, 'Главная страница'),
(12, 'glavnaya-stranica.advantages', 'Наши преимущества', 'Почему именно IT PARK?', NULL, 'text_area', 7, 'Главная страница'),
(14, 'portfolio.portfolio', 'Портфолио', 'Подходим к созданию Вашего проекта с применением ярких и эффективных решений', NULL, 'text_area', 8, 'Портфолио'),
(16, 'portfolio.logos', 'Логотипы', 'Разработка логотипов и Фирменного стиля', NULL, 'text', 9, 'Портфолио'),
(17, 'portfolio.landing', 'LandingPage', NULL, NULL, 'text_area', 10, 'Портфолио'),
(19, 'glavnaya-stranica.integrate', 'Интеграция с сервисами', 'Эксклюзивно создаём дизайн любой сложности', NULL, 'text_area', 12, 'Главная страница'),
(20, 'glavnaya-stranica.multilang', 'Мультиязычность', 'С самого начала и до завершения проекта, разработчики занимаются только Вами', NULL, 'text_area', 13, 'Главная страница'),
(21, 'glavnaya-stranica.complex_foms', 'Сложные формы', 'Сделаем версию сайта на иностранном языке, которая будет генерировать поток новых клиентов.', NULL, 'text_area', 15, 'Главная страница'),
(22, 'glavnaya-stranica.individual', 'Индивидуальные решения', 'Качество реализации проекта и техническая поддержка проекта гарантированы', NULL, 'text_area', 14, 'Главная страница'),
(23, 'komanda.team', 'Наша команда', 'Подходим к созданию Вашего проекта с применением ярких и эффективных решений', NULL, 'text_area', 16, 'Команда'),
(24, 'komanda.philosophy', 'Наша философия', 'В нашем ремесле нет слова \" невозможно\".\r\nБоитесь того, что Ваша идея труднореализуема? Дизайн будет несовременным?\r\nЕсли Вас посещали эти вопросы-Вашим переживаниям пришел конец.', NULL, 'text_area', 17, 'Команда'),
(25, 'komanda.ideas', 'Продуманность', 'Разрабатывая проект, мы учитываем все сценарии действий пользователя. Куда он нажмет, куда попадет, что захочет сделать дальше. Так мы делаем интерфейсы, в которых не придется часами искать нужную страницу. А еще — приятный дизайн, \r\nне вызывающий желания «развидеть это», и наглядное представление аналитики. Даже если ее слишком много.', NULL, 'text_area', 18, 'Команда'),
(26, 'uslugi.best-offer', 'Лучшее наше предложение', NULL, NULL, 'text_area', 19, 'Услуги'),
(27, 'stati.articles', 'Статьи', 'Подходим к созданию Вашего проекта с применением ярких и эффективных решений', NULL, 'text_area', 20, 'Статьи'),
(28, 'glavnaya-stranica.welcome', 'Добро пожаловать', 'Создание уникальных IT-проектов\r\nиного уровня', NULL, 'text_area', 21, 'Главная страница'),
(29, 'glavnaya-stranica.welcome_desc', 'Добро пожаловать: описание', 'Самые современные технологии дизайна и кодинга', NULL, 'text_area', 22, 'Главная страница'),
(30, 'glavnaya-stranica.doing', 'Чем занимается наша компания', 'Что мы создаём', NULL, 'text_area', 23, 'Главная страница'),
(31, 'glavnaya-stranica.doing_desc', 'Чем занимается наша компания: описания', 'Подходим к созданию Вашего проекта с применением ярких и эффективных решений', NULL, 'text_area', 24, 'Главная страница'),
(32, 'glavnaya-stranica.footer', 'Футер', NULL, NULL, 'text_area', 25, 'Главная страница'),
(33, 'glavnaya-stranica.clients', 'Клиенты', 'Наши клиенты', NULL, 'text_area', 26, 'Главная страница'),
(34, 'glavnaya-stranica.clients_desc', 'Клиенты: описание', NULL, NULL, 'text_area', 27, 'Главная страница'),
(35, 'glavnaya-stranica.title_info', 'Информация о компании', 'О нас', NULL, 'text_area', 28, 'Главная страница'),
(36, 'glavnaya-stranica.desc_info', 'Информация о компании: описание', NULL, NULL, 'text_area', 29, 'Главная страница'),
(43, 'glavnaya-stranica.advantage-first', 'Первое', 'Престиж', NULL, 'text', 36, 'Главная страница'),
(44, 'glavnaya-stranica.advantage-second', 'Второе', 'Быстрота', NULL, 'text', 37, 'Главная страница'),
(45, 'glavnaya-stranica.advantage-third', 'Третье', 'Фишки', NULL, 'text', 38, 'Главная страница'),
(48, 'glavnaya-stranica.advantage-fourth', 'Четвертое', 'Результат', NULL, 'text', 39, 'Главная страница'),
(50, 'uslugi.offers_desc', 'Заголовок услуг: описание', NULL, NULL, 'text_area', 41, 'Услуги'),
(51, 'uslugi.offers_title', 'Заголовок услуг', NULL, NULL, 'text', 42, 'Услуги'),
(52, 'uslugi.offers_sub_desc', 'Под описание', NULL, NULL, 'text_area', 43, 'Услуги'),
(53, 'uslugi.best-offer_title', 'Заголовок предложении', NULL, NULL, 'text', 44, 'Услуги'),
(54, 'uslugi.offers_title_each', 'Виды услуг: заголовок', NULL, NULL, 'text', 45, 'Услуги'),
(55, 'glavnaya-stranica.about_us_image', 'О нас: фото', 'settings/April2021/I2gtMh302X9m8vAWv67f.jpg', NULL, 'image', 46, 'Главная страница'),
(58, 'glavnaya-stranica-en.info', 'О нашей компании', 'We are a team of professionals working for the result.\r\nWanna take your business to the next level?\r\nJust call us', NULL, 'text_area', 48, 'Главная страница en'),
(59, 'glavnaya-stranica-en.advantages', 'Наши преимущества', 'Advantages you get by working with us', NULL, 'text_area', 49, 'Главная страница en'),
(60, 'glavnaya-stranica-en.integrate', 'Интеграция с сервисами', 'We exclusively create designs', NULL, 'text_area', 50, 'Главная страница en'),
(61, 'glavnaya-stranica-en.multilang', 'Мультиязычность', 'From the very beginning till the end of the project the developers are only concerned with you', NULL, 'text_area', 51, 'Главная страница en'),
(62, 'glavnaya-stranica-en.individual', 'Индивидуальные решения', 'The quality of project implementation and technical support of the project are guaranteed', NULL, 'text_area', 52, 'Главная страница en'),
(63, 'glavnaya-stranica-en.complex_foms', 'Сложные формы', 'We will make a version of the site in a foreign language, which will generate a new stream of customers.', NULL, 'text_area', 53, 'Главная страница en'),
(64, 'glavnaya-stranica-en.welcome', 'Добро пожаловать', 'We have fun. And build apps, websites, software', NULL, 'text', 54, 'Главная страница en'),
(65, 'glavnaya-stranica-en.welcome_desc', 'Добро пожаловать: описание', NULL, NULL, 'text_area', 55, 'Главная страница en'),
(66, 'glavnaya-stranica-en.doing', 'Чем занимается наша компания', 'What we do', NULL, 'text', 56, 'Главная страница en'),
(67, 'glavnaya-stranica-en.doing_desc', 'Чем занимается наша компания: описания', NULL, NULL, 'text_area', 57, 'Главная страница en'),
(68, 'glavnaya-stranica-en.footer', 'Футер', NULL, NULL, 'text', 58, 'Главная страница en'),
(69, 'glavnaya-stranica-en.clients', 'Клиенты', 'Our clients', NULL, 'text', 59, 'Главная страница en'),
(70, 'glavnaya-stranica-en.clients_desc', 'Клиенты: описание', NULL, NULL, 'text_area', 60, 'Главная страница en'),
(71, 'glavnaya-stranica-en.title_info', 'Информация о компании', 'About us', NULL, 'text', 61, 'Главная страница en'),
(72, 'glavnaya-stranica-en.desc_info', 'Информация о компании: описание', NULL, NULL, 'text_area', 62, 'Главная страница en'),
(73, 'glavnaya-stranica-en.advantage-first', 'Первое', 'Prestige', NULL, 'text', 63, 'Главная страница en'),
(74, 'glavnaya-stranica-en.advantage-second', 'Второе', 'Speed', NULL, 'text', 64, 'Главная страница en'),
(75, 'glavnaya-stranica-en.advantage-third', 'Третье', 'Gifts', NULL, 'text', 65, 'Главная страница en'),
(76, 'glavnaya-stranica-en.advantage-fourth', 'Четвертое', 'Result', NULL, 'text', 66, 'Главная страница en'),
(77, 'portfolio-en.portfolio', 'Портфолио', 'We approach the creation of your project using bright and effective solutions', NULL, 'text_area', 67, 'Портфолио en'),
(78, 'portfolio-en.logos', 'Логотипы', 'Logos and Corporate identity', NULL, 'text', 68, 'Портфолио en'),
(79, 'portfolio-en.landing', 'LandingPage', NULL, NULL, 'text_area', 69, 'Портфолио en'),
(80, 'komanda-en.team', 'Наша команда', 'We approach the creation of your project using bright and effective solutions', NULL, 'text_area', 70, 'Команда en'),
(81, 'komanda-en.philosophy', 'Наша философия', 'There is no word \"impossible\" in our philosophy.Are you afraid that your idea is difficult to implement? Just call us', NULL, 'text_area', 71, 'Команда en'),
(82, 'komanda-en.ideas', 'Продуманность', 'When developing a project, we take into account all scenarios of user actions. Where he\'ll click, where he\'ll go, what he wants to do next. This is how we create interfaces in which you don\'t have to spend hours looking for the desired page. And also a nice design,\r\nnot causing the desire to \"unsee it\", and a visual representation of analytics. Even if there is too much of it.', NULL, 'text_area', 72, 'Команда en'),
(83, 'uslugi-en.best-offer', 'Лучшее наше предложение', NULL, NULL, 'text_area', 73, 'Услуги en'),
(85, 'uslugi-en.offers_desc', 'Заголовок услуг: описание', NULL, NULL, 'text_area', 74, 'Услуги en'),
(86, 'uslugi-en.offers_title', 'Заголовок услуг', NULL, NULL, 'text', 75, 'Услуги en'),
(87, 'uslugi-en.offers_sub_desc', 'Под описание', NULL, NULL, 'text_area', 76, 'Услуги en'),
(88, 'uslugi-en.best-offer_title', 'Заголовок предложении', NULL, NULL, 'text', 77, 'Услуги en'),
(89, 'uslugi-en.offers_title_each', 'Виды услуг: заголовок', NULL, NULL, 'text', 78, 'Услуги en'),
(90, 'stati-en.articles', 'Статьи', NULL, NULL, 'text_area', 79, 'Статьи en'),
(91, 'glavnaya-stranica-kz.advantages', 'Наши преимущества', '', NULL, 'text', 80, 'Главная страница kz'),
(92, 'glavnaya-stranica-kz.info', 'О нашей компании', '', NULL, 'text_area', 81, 'Главная страница kz'),
(93, 'glavnaya-stranica-kz.integrate', 'Интеграция с сервисами', '', NULL, 'text_area', 82, 'Главная страница kz'),
(94, 'glavnaya-stranica-kz.multilang', 'Мультиязычность', '', NULL, 'text', 83, 'Главная страница kz'),
(95, 'glavnaya-stranica-kz.individual', 'Индивидуальные решения', '', NULL, 'text_area', 84, 'Главная страница kz'),
(96, 'glavnaya-stranica-kz.complex_foms', 'Сложные формы', '', NULL, 'text_area', 85, 'Главная страница kz'),
(97, 'glavnaya-stranica-kz.welcome', 'Добро пожаловать', '', NULL, 'text', 86, 'Главная страница kz'),
(98, 'glavnaya-stranica-kz.welcome_desc', 'Добро пожаловать: описание', '', NULL, 'text_area', 87, 'Главная страница kz'),
(99, 'glavnaya-stranica-kz.doing', 'Чем занимается наша компания', '', NULL, 'text', 88, 'Главная страница kz'),
(100, 'glavnaya-stranica-kz.doing_desc', 'Чем занимается наша компания: описания', '', NULL, 'text_area', 89, 'Главная страница kz'),
(101, 'glavnaya-stranica-kz.footer', 'Футер', '', NULL, 'text', 90, 'Главная страница kz'),
(102, 'glavnaya-stranica-kz.clients', 'Клиенты', '', NULL, 'text', 91, 'Главная страница kz'),
(103, 'glavnaya-stranica-kz.clients_desc', 'Клиенты: описание', '', NULL, 'text_area', 92, 'Главная страница kz'),
(104, 'glavnaya-stranica-kz.title_info', 'Информация о компании', '', NULL, 'text', 93, 'Главная страница kz'),
(105, 'glavnaya-stranica-kz.desc_info', 'Информация о компании: описание', '', NULL, 'text_area', 94, 'Главная страница kz'),
(106, 'glavnaya-stranica-kz.advantage-first', 'Первое', '', NULL, 'text', 95, 'Главная страница kz'),
(107, 'glavnaya-stranica-kz.advantage-second', 'Второе', '', NULL, 'text', 96, 'Главная страница kz'),
(108, 'glavnaya-stranica-kz.advantage-third', 'Третье', '', NULL, 'text', 97, 'Главная страница kz'),
(109, 'glavnaya-stranica-kz.advantage-fourth', 'Четвертое', '', NULL, 'text', 98, 'Главная страница kz'),
(110, 'portfolio-kz.portfolio', 'Портфолио', '', NULL, 'text_area', 99, 'Портфолио kz'),
(111, 'portfolio-kz.logos', 'Логотипы', '', NULL, 'text', 100, 'Портфолио kz'),
(112, 'portfolio-kz.landing', 'LandingPage', '', NULL, 'text_area', 101, 'Портфолио kz'),
(115, 'komanda-kz.team', 'Наша команда', '', NULL, 'text', 102, 'Команда kz'),
(116, 'komanda-kz.philosophy', 'Наша философия', '', NULL, 'text_area', 103, 'Команда kz'),
(117, 'komanda-kz.ideas', 'Продуманность', '', NULL, 'text_area', 104, 'Команда kz'),
(118, 'uslugi-kz.best-offer', 'Лучшее наше предложение', '', NULL, 'text_area', 105, 'Услуги kz'),
(119, 'uslugi-kz.offers_desc', 'Заголовок услуг: описание', '', NULL, 'text_area', 106, 'Услуги kz'),
(120, 'uslugi-kz.offers_title', 'Заголовок услуг', '', NULL, 'text', 107, 'Услуги kz'),
(121, 'uslugi-kz.offers_sub_desc', 'Под описание', '', NULL, 'text_area', 108, 'Услуги kz'),
(122, 'uslugi-kz.best-offer_title', 'Заголовок предложении', '', NULL, 'text', 109, 'Услуги kz'),
(123, 'uslugi-kz.offers_title_each', 'Виды услуг: заголовок', '', NULL, 'text', 110, 'Услуги kz'),
(124, 'stati-kz.articles', 'Статьи', '', NULL, 'text_area', 111, 'Статьи kz');

-- --------------------------------------------------------

--
-- Структура таблицы `sub_offers`
--

CREATE TABLE `sub_offers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `offer_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '450',
  `best_offer` tinyint(1) NOT NULL DEFAULT 0,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `sub_offers`
--

INSERT INTO `sub_offers` (`id`, `offer_id`, `title`, `created_at`, `updated_at`, `price`, `best_offer`, `description`) VALUES
(1, 1, 'UX/UI Дизайн', '2021-03-30 06:30:25', '2021-04-22 23:49:48', '450', 0, ''),
(2, 1, 'Логотипы', '2021-04-02 00:01:49', '2021-04-22 23:49:28', '450', 0, ''),
(3, 1, 'Дизайн мобильных приложений', '2021-04-02 00:02:36', '2021-04-22 23:49:16', '450', 0, ''),
(4, 1, 'Рекламные баннера', '2021-04-02 00:02:57', '2021-04-22 23:48:55', '450', 0, ''),
(5, 3, 'Системы безопасности', '2021-04-02 00:03:18', '2021-04-22 23:48:40', '450', 0, ''),
(6, 3, 'Оптимизация бизнеса', '2021-04-02 00:03:40', '2021-04-22 23:48:24', '450', 0, ''),
(7, 3, 'Для складских учетов', '2021-04-02 00:04:03', '2021-04-22 23:48:07', '450', 0, ''),
(8, 3, 'Виртуальная реальность', '2021-04-02 00:24:48', '2021-04-22 23:47:45', '450', 0, ''),
(9, 3, 'SMM', '2021-04-02 00:25:10', '2021-04-23 00:35:13', '1', 1, ''),
(10, 4, 'Landing pages', '2021-04-02 00:27:19', '2021-04-22 23:51:44', '450', 0, ''),
(11, 4, 'Корпоративные сайты', '2021-04-02 00:27:42', '2021-04-22 23:47:30', '450', 0, ''),
(12, 4, 'Интернет магазины', '2021-04-02 00:27:55', '2021-04-22 23:47:10', '450', 0, ''),
(13, 4, 'Веб порталы', '2021-04-02 00:28:08', '2021-04-22 23:46:52', '450', 0, ''),
(14, 4, 'CRM системы', '2021-04-02 00:28:34', '2021-04-22 23:46:40', '450', 0, ''),
(16, 4, 'Системы управления (CMS)', '2021-04-02 00:28:57', '2021-04-22 23:46:29', '450', 0, ''),
(17, 4, 'Тестирование сайтов', '2021-04-02 00:29:07', '2021-04-22 23:46:14', '450', 0, ''),
(18, 4, 'Подключение интеграций', '2021-04-02 00:38:28', '2021-04-22 23:45:52', '450', 0, ''),
(19, 5, 'Контекстная реклама', '2021-04-02 00:42:00', '2021-04-22 23:45:36', '450', 0, ''),
(20, 5, 'Таргетированная реклама ', '2021-04-02 00:42:20', '2021-04-22 23:52:13', '450', 0, ''),
(21, 5, 'Маркетинговый анализ', '2021-04-02 00:44:37', '2021-04-22 23:45:15', '450', 0, ''),
(22, 5, 'Социальные сети', '2021-04-02 00:44:52', '2021-04-22 23:44:58', '450', 0, ''),
(23, 7, 'Маркетплейсы', '2021-04-02 00:46:10', '2021-04-22 23:44:46', '450', 0, ''),
(24, 7, 'Обучающие платформы', '2021-04-02 00:46:33', '2021-04-23 00:35:28', '2', 1, ''),
(25, 7, 'Служба доставки', '2021-04-02 00:46:50', '2021-04-22 23:44:18', '450', 0, ''),
(26, 7, 'Новостные порталы', '2021-04-02 00:47:15', '2021-04-21 05:27:24', '450', 0, ''),
(27, 7, 'Landing', '2021-04-02 00:47:26', '2021-04-23 00:24:21', '450', 0, 'Разработаем продающий, уникальный \"одностраничник\" под ключ');

-- --------------------------------------------------------

--
-- Структура таблицы `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2021-03-30 05:37:10', '2021-03-30 05:37:10'),
(31, 'offers', 'title', 7, 'en', 'Mobile apps', '2021-04-21 03:32:02', '2021-04-21 03:32:02'),
(32, 'offers', 'description', 7, 'en', 'We implement any idea of ​​a promising startup', '2021-04-21 03:32:02', '2021-04-23 00:20:34'),
(33, 'sub_offers', 'title', 26, 'en', 'News portals', '2021-04-21 05:27:24', '2021-04-22 23:50:52'),
(34, 'projects', 'title', 4, 'en', 'Your Puzzles', '2021-04-22 22:57:23', '2021-04-22 22:57:23'),
(35, 'projects', 'description', 4, 'en', 'Your Puzzles ', '2021-04-22 22:57:23', '2021-04-22 22:57:23'),
(36, 'projects', 'title', 3, 'en', 'La Jole', '2021-04-22 22:57:43', '2021-04-22 22:57:43'),
(37, 'projects', 'description', 3, 'en', 'La Jole', '2021-04-22 22:57:43', '2021-04-22 22:57:43'),
(38, 'projects', 'title', 2, 'en', 'Максимальная Защита', '2021-04-22 22:57:57', '2021-04-22 22:57:57'),
(39, 'projects', 'description', 2, 'en', '\"Максимальная Защита\"', '2021-04-22 22:57:57', '2021-04-22 22:57:57'),
(40, 'projects', 'title', 1, 'en', 'LandingPage - Wellnessа', '2021-04-22 22:58:05', '2021-04-22 22:58:05'),
(41, 'projects', 'description', 1, 'en', '\"Wellnes\"', '2021-04-22 22:58:05', '2021-04-22 22:58:05'),
(42, 'posts', 'title', 1, 'en', 'Online store', '2021-04-22 23:33:27', '2021-04-22 23:33:27'),
(43, 'posts', 'excerpt', 1, 'en', 'An online store is a great sales tool for your brand. Because.....', '2021-04-22 23:33:27', '2021-04-22 23:33:27'),
(44, 'posts', 'body', 1, 'en', '<p>An online store is a great sales tool for your brand. Because...</p>', '2021-04-22 23:33:27', '2021-04-28 22:57:16'),
(45, 'posts', 'slug', 1, 'en', 'sozdanie-internet-magazina', '2021-04-22 23:33:27', '2021-04-28 22:57:16'),
(46, 'posts', 'meta_description', 1, 'en', 'This is the meta description', '2021-04-22 23:33:27', '2021-04-22 23:33:27'),
(47, 'posts', 'meta_keywords', 1, 'en', 'keyword1, keyword2, keyword3', '2021-04-22 23:33:27', '2021-04-22 23:33:27'),
(48, 'sub_offers', 'title', 27, 'en', 'And more', '2021-04-22 23:42:33', '2021-04-22 23:51:06'),
(49, 'sub_offers', 'description', 27, 'en', 'Разработаем продающий, уникальный \"одностраничник\" под ключ', '2021-04-22 23:42:33', '2021-04-22 23:42:33'),
(50, 'sub_offers', 'title', 25, 'en', 'Delivery services', '2021-04-22 23:44:18', '2021-04-22 23:51:35'),
(51, 'sub_offers', 'title', 24, 'en', ' Learning platforms', '2021-04-22 23:44:34', '2021-04-22 23:44:34'),
(52, 'sub_offers', 'title', 23, 'en', ' Marketplaces', '2021-04-22 23:44:46', '2021-04-22 23:44:46'),
(53, 'sub_offers', 'title', 22, 'en', 'Social Medias', '2021-04-22 23:44:58', '2021-04-22 23:52:58'),
(54, 'sub_offers', 'title', 21, 'en', 'Marketing Analysis', '2021-04-22 23:45:15', '2021-04-22 23:45:15'),
(55, 'sub_offers', 'title', 20, 'en', 'Facebook ads', '2021-04-22 23:45:26', '2021-04-22 23:52:13'),
(56, 'sub_offers', 'title', 19, 'en', 'Google ads', '2021-04-22 23:45:36', '2021-04-22 23:45:36'),
(57, 'sub_offers', 'title', 18, 'en', 'Integrations', '2021-04-22 23:45:52', '2021-04-22 23:45:52'),
(58, 'sub_offers', 'title', 17, 'en', ' Website testing', '2021-04-22 23:46:14', '2021-04-22 23:46:14'),
(59, 'sub_offers', 'title', 16, 'en', 'CMS', '2021-04-22 23:46:29', '2021-04-22 23:46:29'),
(60, 'sub_offers', 'title', 14, 'en', 'CRM ', '2021-04-22 23:46:40', '2021-04-22 23:46:40'),
(61, 'sub_offers', 'title', 13, 'en', ' Web portals', '2021-04-22 23:46:52', '2021-04-22 23:46:52'),
(62, 'sub_offers', 'title', 12, 'en', 'Online stores', '2021-04-22 23:47:10', '2021-04-22 23:47:10'),
(63, 'sub_offers', 'title', 11, 'en', 'Corporate sites', '2021-04-22 23:47:30', '2021-04-22 23:47:30'),
(64, 'sub_offers', 'title', 8, 'en', 'VR', '2021-04-22 23:47:45', '2021-04-22 23:47:45'),
(65, 'sub_offers', 'title', 7, 'en', 'Warehouse software', '2021-04-22 23:48:07', '2021-04-22 23:48:07'),
(66, 'sub_offers', 'title', 6, 'en', 'Business optimization', '2021-04-22 23:48:24', '2021-04-22 23:48:24'),
(67, 'sub_offers', 'title', 5, 'en', 'Security systems', '2021-04-22 23:48:40', '2021-04-22 23:48:40'),
(68, 'sub_offers', 'title', 4, 'en', 'Advertising banners', '2021-04-22 23:48:55', '2021-04-22 23:48:55'),
(69, 'sub_offers', 'title', 3, 'en', 'Mobile Apps Design', '2021-04-22 23:49:16', '2021-04-22 23:49:16'),
(70, 'sub_offers', 'title', 2, 'en', 'Logos', '2021-04-22 23:49:28', '2021-04-22 23:49:28'),
(71, 'sub_offers', 'title', 1, 'en', 'UX/UI Design', '2021-04-22 23:49:48', '2021-04-22 23:49:48'),
(72, 'sub_offers', 'title', 10, 'en', 'Landing pages', '2021-04-22 23:51:44', '2021-04-22 23:51:44'),
(73, 'reviews', 'review', 1, 'en', 'The company provided just excellent service and from the quality of the site at the highest level', '2021-04-22 23:54:03', '2021-04-22 23:54:03'),
(74, 'contacts', 'address', 1, 'en', '13к2в Al-Farabi street , Almaty 050059, Kazakhstan', '2021-04-22 23:57:19', '2021-04-22 23:57:19'),
(75, 'offers', 'title', 4, 'en', 'Websites', '2021-04-23 00:05:13', '2021-04-23 00:05:13'),
(76, 'offers', 'description', 4, 'en', 'We develop websites without overloads, bugs and with correct mobile adaptation.', '2021-04-23 00:05:13', '2021-04-23 00:19:46'),
(77, 'offers', 'title', 3, 'en', 'Softwares', '2021-04-23 00:05:24', '2021-04-23 00:05:24'),
(78, 'offers', 'description', 3, 'en', 'We develop great softwares to optimize your business.', '2021-04-23 00:05:24', '2021-04-23 00:18:30'),
(79, 'offers', 'title', 1, 'en', 'Design', '2021-04-23 00:05:45', '2021-04-23 00:05:45'),
(80, 'offers', 'description', 1, 'en', 'We will create a unique UI / UX design.', '2021-04-23 00:05:45', '2021-04-23 00:17:58'),
(81, 'offers', 'title', 5, 'en', 'SMM', '2021-04-23 00:20:01', '2021-04-23 00:20:01'),
(82, 'offers', 'description', 5, 'en', 'We will increase the flow of customers for your brand.', '2021-04-23 00:20:01', '2021-04-23 00:20:15'),
(83, 'guarantees', 'title', 7, 'en', 'Domain and hosting', '2021-04-23 00:29:02', '2021-04-23 00:29:02'),
(84, 'guarantees', 'title', 6, 'en', 'CMS', '2021-04-23 00:30:29', '2021-04-23 00:30:29'),
(85, 'guarantees', 'title', 5, 'en', 'Front-end', '2021-04-23 00:31:02', '2021-04-23 00:31:02'),
(86, 'guarantees', 'title', 4, 'en', 'Design', '2021-04-23 00:31:48', '2021-04-23 00:31:48'),
(87, 'guarantees', 'title', 2, 'en', 'Google ads', '2021-04-23 00:32:31', '2021-04-23 00:32:31'),
(88, 'guarantees', 'title', 1, 'en', 'Facebook ads', '2021-04-23 00:32:40', '2021-04-23 00:32:40'),
(89, 'guarantees', 'title', 3, 'en', 'Logos', '2021-04-23 00:34:16', '2021-04-23 00:34:16'),
(90, 'sub_offers', 'title', 9, 'en', 'SMM', '2021-04-23 00:35:13', '2021-04-23 00:35:13'),
(91, 'sub_offers', 'description', 9, 'en', '   ', '2021-04-23 00:35:13', '2021-04-23 00:36:06'),
(92, 'posts', 'title', 2, 'en', 'Landing or web-site - which one is better?', '2021-04-26 03:21:12', '2021-04-26 03:24:34'),
(93, 'posts', 'excerpt', 2, 'en', 'There are many different types of sites to be found on the internet. All of them are developed for specific purposes and tasks ...', '2021-04-26 03:21:12', '2021-04-26 03:24:34'),
(94, 'posts', 'body', 2, 'en', '<p>This is the body for the sample post, which includes the body. We can use all kinds of format! And include a bunch of other stuff.</p>', '2021-04-26 03:21:12', '2021-04-28 22:58:08'),
(95, 'posts', 'slug', 2, 'en', 'lending-ili-mnogostranichnyj-sajt', '2021-04-26 03:21:12', '2021-04-28 22:58:08'),
(96, 'posts', 'meta_description', 2, 'en', 'Meta Description for sample post', '2021-04-26 03:21:12', '2021-04-26 03:21:12'),
(97, 'posts', 'meta_keywords', 2, 'en', 'keyword1, keyword2, keyword3', '2021-04-26 03:21:12', '2021-04-26 03:21:12'),
(98, 'posts', 'title', 3, 'en', 'How the cost of your project is formed', '2021-04-26 03:21:18', '2021-04-26 03:26:35'),
(99, 'posts', 'excerpt', 3, 'en', 'The standard site development scheme in any large Internet company includes several basic ...', '2021-04-26 03:21:18', '2021-04-26 03:26:35'),
(100, 'posts', 'body', 3, 'en', '<p>This is the body for the latest post</p>', '2021-04-26 03:21:18', '2021-04-26 03:21:18'),
(101, 'posts', 'slug', 3, 'en', 'seo-trendy-2020-i-sovety-po-ih-primeneniyu', '2021-04-26 03:21:18', '2021-04-28 22:58:35'),
(102, 'posts', 'meta_description', 3, 'en', 'This is the meta description', '2021-04-26 03:21:18', '2021-04-26 03:21:18'),
(103, 'posts', 'meta_keywords', 3, 'en', 'keyword1, keyword2, keyword3', '2021-04-26 03:21:18', '2021-04-26 03:21:18'),
(105, 'crews', 'description', 5, 'en', 'There are no non-selling ideas, there are only non-selling actions.\nMy task is to implement the first', '2021-04-26 23:44:07', '2021-04-26 23:44:07'),
(106, 'crews', 'description', 6, 'en', 'Как говорит моя мама: \"Балам, Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae cumque delectus dolore ea et eum expedita fugit incidunt, labore laudantium nulla, odit omnis perspiciatis quibusdam quis ut voluptate. Modi, nulla екенін ұмытушы болма.\"', '2021-04-27 02:00:16', '2021-04-27 02:00:16'),
(107, 'posts', 'seo_title', 1, 'en', 'разработка сайта', '2021-04-27 03:01:14', '2021-04-27 03:01:14'),
(108, 'posts', 'title', 5, 'en', 'Конструктор Меню', '2021-04-27 03:06:02', '2021-04-27 03:06:02'),
(109, 'posts', 'excerpt', 5, 'en', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus delectus dolorum illo iusto magni minus repellendus vitae voluptatum. Animi asperiores at blanditiis incidunt nesciunt ratione sed vitae? Qui, vitae.', '2021-04-27 03:06:02', '2021-04-27 03:06:02'),
(110, 'posts', 'body', 5, 'en', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus delectus dolorum illo iusto magni minus repellendus vitae voluptatum. Animi asperiores at blanditiis incidunt nesciunt ratione sed vitae? Qui, vitae. Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus delectus dolorum illo iusto magni minus repellendus vitae voluptatum. Animi asperiores at blanditiis incidunt nesciunt ratione sed vitae? Qui, vitae. Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus delectus dolorum illo iusto magni minus repellendus vitae voluptatum. Animi asperiores at blanditiis incidunt nesciunt ratione sed vitae? Qui, vitae.</p>', '2021-04-27 03:06:02', '2021-04-28 22:55:14'),
(111, 'posts', 'slug', 5, 'en', 'optimizaciya-kartinok-na-sajte', '2021-04-27 03:06:02', '2021-04-28 22:55:14'),
(112, 'posts', 'title', 4, 'en', 'Yarr Post', '2021-04-27 03:12:03', '2021-04-27 03:12:03'),
(113, 'posts', 'excerpt', 4, 'en', 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '2021-04-27 03:12:03', '2021-04-27 03:12:03'),
(114, 'posts', 'body', 4, 'en', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder. Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon. Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', '2021-04-27 03:12:03', '2021-04-28 22:58:52'),
(115, 'posts', 'slug', 4, 'en', 'kak-formiruetsya-stoimost-sajta-i-pochemu-luchshe-zakazyvat-ego-razrabotku-cherez-agentstvo', '2021-04-27 03:12:03', '2021-04-28 22:58:52'),
(116, 'posts', 'meta_description', 4, 'en', 'this be a meta descript', '2021-04-27 03:12:03', '2021-04-27 03:12:03'),
(117, 'posts', 'meta_keywords', 4, 'en', 'keyword1, keyword2, keyword3', '2021-04-27 03:12:03', '2021-04-27 03:12:03'),
(118, 'crews', 'description', 7, 'en', 'Для меня разработка - это некий стиль жизни. Когда дело доходит до работы, я теряю счет времени и полностью погружаюсь в \"коде\". ', '2021-04-27 03:27:22', '2021-04-27 03:27:22'),
(119, 'crews', 'description', 9, 'en', 'Слаженность команды и позитивный настрой являются моей главной обязанностью ', '2021-04-27 03:53:15', '2021-04-27 03:53:15'),
(120, 'data_rows', 'display_name', 29, 'en', 'ID', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(121, 'data_rows', 'display_name', 30, 'en', 'Author', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(122, 'data_rows', 'display_name', 31, 'en', 'Category', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(123, 'data_rows', 'display_name', 32, 'en', 'Title', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(124, 'data_rows', 'display_name', 42, 'en', 'SEO Title', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(125, 'data_rows', 'display_name', 33, 'en', 'Excerpt', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(126, 'data_rows', 'display_name', 34, 'en', 'Body', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(127, 'data_rows', 'display_name', 35, 'en', 'Post Image', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(128, 'data_rows', 'display_name', 36, 'en', 'Slug', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(129, 'data_rows', 'display_name', 37, 'en', 'Meta Description', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(130, 'data_rows', 'display_name', 38, 'en', 'Meta Keywords', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(131, 'data_rows', 'display_name', 39, 'en', 'Status', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(132, 'data_rows', 'display_name', 43, 'en', 'Featured', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(133, 'data_rows', 'display_name', 40, 'en', 'Created At', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(134, 'data_rows', 'display_name', 41, 'en', 'Updated At', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(135, 'data_rows', 'display_name', 70, 'en', 'Is Popular', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(136, 'data_rows', 'display_name', 71, 'en', 'Main Page', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(137, 'data_types', 'display_name_singular', 5, 'en', 'Post', '2021-04-27 22:36:41', '2021-04-27 22:36:41'),
(138, 'data_types', 'display_name_plural', 5, 'en', 'Posts', '2021-04-27 22:36:41', '2021-04-27 22:36:41');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$mKTW7kwTqcHV8auCxZ81kOGF9nZuRDCiatWtxt9.30jE5Eo2goHZS', 'MLXxTHdhAjpW0t1mPxjmUfsyCJsfrTl6wGSDhjRufCaW5Bhg7Yh969oBMU3c', '{\"locale\":\"ru\"}', '2021-03-30 05:37:10', '2021-04-13 00:59:24');

-- --------------------------------------------------------

--
-- Структура таблицы `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `welcome_titles`
--

CREATE TABLE `welcome_titles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `advantages`
--
ALTER TABLE `advantages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Индексы таблицы `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `crews`
--
ALTER TABLE `crews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Индексы таблицы `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Индексы таблицы `guarantees`
--
ALTER TABLE `guarantees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `guarantees_sub_offer_id_foreign` (`sub_offer_id`);

--
-- Индексы таблицы `infos`
--
ALTER TABLE `infos`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Индексы таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Индексы таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Индексы таблицы `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `projects_slug_unique` (`slug`),
  ADD KEY `projects_project_type_id_foreign` (`project_type_id`);

--
-- Индексы таблицы `project_types`
--
ALTER TABLE `project_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Индексы таблицы `sub_offers`
--
ALTER TABLE `sub_offers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_offers_offer_id_foreign` (`offer_id`);

--
-- Индексы таблицы `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Индексы таблицы `welcome_titles`
--
ALTER TABLE `welcome_titles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `advantages`
--
ALTER TABLE `advantages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `contents`
--
ALTER TABLE `contents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `crews`
--
ALTER TABLE `crews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT для таблицы `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `guarantees`
--
ALTER TABLE `guarantees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `infos`
--
ALTER TABLE `infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT для таблицы `offers`
--
ALTER TABLE `offers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `project_types`
--
ALTER TABLE `project_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT для таблицы `sub_offers`
--
ALTER TABLE `sub_offers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `welcome_titles`
--
ALTER TABLE `welcome_titles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `guarantees`
--
ALTER TABLE `guarantees`
  ADD CONSTRAINT `guarantees_sub_offer_id_foreign` FOREIGN KEY (`sub_offer_id`) REFERENCES `sub_offers` (`id`);

--
-- Ограничения внешнего ключа таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_project_type_id_foreign` FOREIGN KEY (`project_type_id`) REFERENCES `project_types` (`id`);

--
-- Ограничения внешнего ключа таблицы `sub_offers`
--
ALTER TABLE `sub_offers`
  ADD CONSTRAINT `sub_offers_offer_id_foreign` FOREIGN KEY (`offer_id`) REFERENCES `offers` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
