<?php

namespace App\Http\Controllers;

use App\Models\HeaderFooter\Contact;
use App\Models\IndexPage\Offer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Page;
use TCG\Voyager\Models\Post;

class PostController extends Controller
{
    public function posts()
    {
        $title = 'IT PARK';
        $descrips = 'Memes';
        $popularPosts = Post::where('is_popular', 1)->get();
        $newPosts = Post::orderBy('created_at', 'desc')->get();
        $posts = Post::inRandomOrder()->get();
        $offers = Offer::with('subOffer')->orderBy('order', 'asc')->get();
        $contacts = Contact::first();
        Carbon::setLocale(session('locale'));

//        dd($popularPosts);

        $popularPosts = $popularPosts->translate(session('locale'));
        $newPosts = $newPosts->translate(session('locale'));
        $posts = $posts->translate(session('locale'));
        $offers = $offers->translate(session('locale'));
        $contacts = $contacts->translate(session('locale'));
        if (session('locale') == null) {
            session()->put('locale', 'ru');
        }

        return view('articles', compact('popularPosts', 'descrips', 'newPosts', 'title', 'posts', 'offers', 'contacts'));
    }

    public function details($slug, Post $posts)
    {
        $posts->find($slug);
        $posts = $posts->translate(session('locale'));
        return view('welcome', compact('posts'));
    }

    public function one(\App\Models\Post $post)
    {
        $post = $post->translate(session('locale'));
        $title = 'IT PARK';
        $descrips = 'Memes';
        $contacts = Contact::first();
        return view('article', compact('title', 'descrips', 'contacts', 'post'));
    }

}
