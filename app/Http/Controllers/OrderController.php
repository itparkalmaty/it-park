<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Models\HeaderFooter\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function order(OrderRequest $request)
    {
        $order = Order::create($request->all());

        Mail::send('mail.mail', ['order' => $order], function ($message) {
            $message->to('itparkalmaty@gmail.com', 'IT Park')->subject('Заявка');
            $message->from('info@itpark.com','itpark.network');
        });

        return response(['data' => $order], 200);
    }
}
