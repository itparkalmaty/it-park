<?php

namespace App\Http\Controllers;

use App\Models\HeaderFooter\Contact;
use App\Models\IndexPage\Client;
use App\Models\IndexPage\Info;
use App\Models\IndexPage\Offer;
use App\Models\IndexPage\Review;
use App\Models\IndexPage\SubOffer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Post;

class IndexController extends Controller
{
    public function index()
    {
        $title = 'Разработка сайтов, мобильных приложений и ПО любой сложности и спецификации | IT PARK';
        $descrips = 'Разработка сайтов, мобильных приложений и ПО любой сложности и спецификации.
        Команда профессионалов создаст современные бизнес-инструменты Вашего бренда в онлайн.
        Самые новейшие технологии кодинга. Интеграция с 1С, Bitrix24, AmoCrm и так далее.';
        $offers = Offer::with('subOffer')->orderBy('order', 'asc')->get();
        $firstPost = Post::where('main_page', 1)->first();
        $secondPost = Post::where('main_page', 1)->skip(1)->first();
        $thirdPost = Post::where('main_page', 1)->skip(2)->first();
//        dd(session('locale'));

        $contacts = Contact::first();
        $clients = Client::get();
        $reviews = Review::get();
        $subOffers = SubOffer::get();

        $firstPost = $firstPost->translate(session('locale'));
        $secondPost = $secondPost->translate(session('locale'));
        $thirdPost = $thirdPost->translate(session('locale'));

        $subOffers = $subOffers->translate(session('locale'));
        $offers = $offers->translate(session('locale'));
        $reviews = $reviews->translate(session('locale'));
        $contacts = $contacts->translate(session('locale'));
        foreach ($offers as $key=>$offer){
            foreach ($offer->subOffer as $index=>$subOffer){
                $offers[$key]->subOffer[$index] = $subOffer->translate(session('locale'));

            }
        }
//        dd($offers);
//        dd(session('locale'));
        return view('index', compact(
            'offers', 'title',
            'descrips',
            'contacts', 'clients',
            'reviews', 'firstPost',
            'secondPost', 'thirdPost')
        );
    }

    public function posts()
    {
        Carbon::setLocale('ru');
        $mainPagePosts = Post::where('main_page', 1)->get();
        $offers = Offer::with('subOffer')->get();

        return view('', compact('mainPagePosts', 'offers'));
    }
}
