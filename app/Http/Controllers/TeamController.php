<?php

namespace App\Http\Controllers;

use App\Models\CompanyInfo\Crew;
use App\Models\HeaderFooter\Contact;
use App\Models\IndexPage\Offer;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function index()
    {
        $title = 'Наша команда | IT PARK';
        $descrips = 'Информация о сотрудниках компании';
        $offers = Offer::with('subOffer')->orderBy('order','asc')->get();
        $teamMates = Crew::get();
        $contacts = Contact::first();


        $offers = $offers->translate(session('locale'));
        $teamMates = $teamMates->translate(session('locale'));
        $contacts = $contacts->translate(session('locale'));

        foreach ($offers as $key=>$offer){
            foreach ($offer->subOffer as $index=>$subOffer){
                $offers[$key]->subOffer[$index] = $subOffer->translate(session('locale'));
            }
        }

        if(session('locale') == null){
            session()->put('locale','ru');
        }

//        dd(session('locale'));

        return view('team', compact('offers', 'title', 'descrips', 'teamMates', 'contacts'));
    }
}
