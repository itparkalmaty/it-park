<?php

namespace App\Http\Controllers;

use App\Models\HeaderFooter\Contact;
use App\Models\IndexPage\Offer;
use App\Models\IndexPage\SubOffer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    public function index()
    {
        $title = 'Список услуг | IT PARK';
        $descrips = 'Подробное описание наших услуг и выгодные предложения';
        $offers = Offer::with('subOffer')->get();
        $subOffers = SubOffer::with('guarantee')->where('best_offer',1)->get();
        $contacts = Contact::first();

        $offers = $offers->translate(session('locale'));
        $subOffers = $subOffers->translate(session('locale'));
        $contacts = $contacts->translate(session('locale'));


        foreach ($offers as $key=>$offer){
            foreach ($offer->subOffer as $index=>$subOffer){
                $offers[$key]->subOffer[$index] = $subOffer->translate(session('locale'));

            }
        }

        foreach ($subOffers as $key=>$subOffer){
            foreach ($subOffer->guarantee as $index=>$guarantee){
                $subOffers[$key]->guarantee[$index] = $guarantee->translate(session('locale'));
            }
        }
//        dd($guarantee);

        if(session('locale') == null){
            session()->put('locale','ru');
        }
//        dd(session('locale'));
        return view('services', compact('offers', 'descrips', 'guarantee', 'title', 'subOffers', 'contacts'));
    }
}
