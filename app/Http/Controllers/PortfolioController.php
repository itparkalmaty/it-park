<?php

namespace App\Http\Controllers;

use App\Models\HeaderFooter\Contact;
use App\Models\IndexPage\Offer;
use App\Models\Portfolio\Project;
use App\Models\Portfolio\ProjectType;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    public function index()
    {
        $title = 'Наши работы | IT PARK';
        $descrips = 'Успешно выполненные заказы нашей компании и отзывы клиентов';
        $projects = Project::with('projectType')->get();
        $projectTypo = ProjectType::with('project')->get();
//        dd($projects);
        $offers = Offer::with('subOffer')->orderBy('order','asc')->get();
        $contacts = Contact::first();

        $projects = $projects->translate(session('locale'));
        $offers = $offers->translate(session('locale'));
        $contacts = $contacts->translate(session('locale'));

        foreach ($offers as $key=>$offer){
            foreach ($offer->subOffer as $index=>$subOffer){
                $offers[$key]->subOffer[$index] = $subOffer->translate(session('locale'));
            }
        }

        if(session('locale') == null){
            session()->put('locale','ru');
        }
        return view('portfolio', compact('projects', 'descrips','offers', 'contacts', 'title', 'projectTypo'));
    }

    public function outside(ProjectType $projectType)
    {

        $title = 'Наши работы | IT PARK';
        $descrips = 'Успешно выполненные заказы нашей компании и отзывы клиентов';
        $projectType = $projectType->translate(session('locale'));
        $projectTypo = ProjectType::get();
        $projectTypo = $projectTypo->translate(session('locale'));
        $projects = Project::where('project_type_id', $projectType->id)->get();
//        dd($projects);
        $contacts = Contact::first();
//        dd($projectType);

        return view('portfolio', compact('title', 'descrips', 'projectType', 'contacts', 'projects', 'projectTypo'));
    }

    public function inside(Project $project)
    {
        $title = 'Наши работы | IT PARK | '.$project->title;
        $descrips = $project->description;
//        dd($project);
        $contacts = Contact::first();
        $projectTypo = ProjectType::get();
        $projectTypo = $projectTypo->translate(session('locale'));
        return view('portfolio-inside', compact('title', 'descrips', 'contacts', 'project', 'projectTypo'));
    }
}
