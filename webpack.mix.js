const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
 mix.js('resources/js/main.js', 'public/js');
 mix.scripts([
    'public/js/main.js'
], 'public/js/all.js');

if (mix.inProduction()) {
    mix.version();
}

    mix.postCss('resources/css/white.css', 'public/css', [
        //
    ]);

    mix.styles([
        'resources/css/fonts.css',
        'resources/css/style.css',
        'resources/css/swiper-bundle.min.css'
    ], 'public/css/all.css');
