<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->post('/api/order',[
            'phone' => '87014800350',
            'date' => '11/11/2011',
            'comment' => 'required'
        ]);

        $response->decodeResponseJson();
        $response->assertStatus(201);
    }
}
