<?php

use App\Http\Controllers\OrderController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\PortfolioController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\ServicesController;
use App\Http\Controllers\PostController;

use Spatie\Sitemap\SitemapGenerator;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('locale/{locale}', [\App\Http\Controllers\Controller::class, 'locale'])->name('locale');

// Pages with localization
Route::middleware(['set_locale'])->group(function (){
    Route::get('/', [IndexController::class, 'index'])->name('index');
    Route::get('/portfolio', [PortfolioController::class, 'index'])->name('portfolio');
    Route::get('/team', [TeamController::class, 'index'])->name('team');
    Route::get('/services', [ServicesController::class, 'index'])->name('services');
    Route::get('/articles', [PostController::class,'posts'])->name('posts');
    Route::get('/article/{post}', [PostController::class,'one'])->name('article');
    Route::get('/portfolio/{projectType}', [PortfolioController::class, 'outside'])->name('outside');
    Route::get('/portfolio/project/{project}', [PortfolioController::class, 'inside'])->name('inside');
});

// Order
Route::post('/order',[OrderController::class,'order'])->name('order');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

// Sitemap
Route::get('sitemap-create-please', function() {
    SitemapGenerator::create('https://itpark.network')->writeToFile('sitemap.xml');
    return 'Карта сайта успешно обновлена!';
});

